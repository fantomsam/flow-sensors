<div align="center"> <img src="../ArtWork/Flow_icon.png" width="150"> </div>

# Flow Sensors Hardware
This directory contains the design files for the hardware components the project.

The Mechanical designed using [FreeCAD](https://www.freecadweb.org/).<br/>
The PCBs designed using [KiCad](https://www.kicad.org/).

## Authors
* **Sam Harry Tzavaras** - *Initial work*

## License
The Hardware design file is licensed under TAPRv1 or later - see the [License](LICENSE) file for details.

