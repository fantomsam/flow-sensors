PCBNEW-LibModule-V1  2021-09-06 06:06:56
# encoding utf-8
Units mm
$INDEX
INDPM5552X200N
$EndINDEX
$MODULE INDPM5552X200N
Po 0 0 0 15 6135a1f0 00000000 ~~
Li INDPM5552X200N
Cd IHLP
Kw Inductor
Sc 0
At SMD
AR 
Op 0 0 0
T0 0 0 1.27 1.27 0 0.254 N V 21 N "L**"
T1 0 0 1.27 1.27 0 0.254 N I 21 N "INDPM5552X200N"
DS -3.625 -2.967 3.625 -2.967 0.05 24
DS 3.625 -2.967 3.625 2.967 0.05 24
DS 3.625 2.967 -3.625 2.967 0.05 24
DS -3.625 2.967 -3.625 -2.967 0.05 24
DS -2.745 -2.59 2.745 -2.59 0.1 24
DS 2.745 -2.59 2.745 2.59 0.1 24
DS 2.745 2.59 -2.745 2.59 0.1 24
DS -2.745 2.59 -2.745 -2.59 0.1 24
DS 2.745 -2.59 -2.745 -2.59 0.2 21
DS -2.745 2.59 2.745 2.59 0.2 21
$PAD
Po -2.35 0
Sh "1" R 2.05 2.7 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 2.35 0
Sh "2" R 2.05 2.7 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE INDPM5552X200N
$EndLIBRARY
