PCBNEW-LibModule-V1  2021-09-06 06:01:53
# encoding utf-8
Units mm
$INDEX
ECS32712512RCTR
$EndINDEX
$MODULE ECS32712512RCTR
Po 0 0 0 15 6135a0c1 00000000 ~~
Li ECS32712512RCTR
Cd ECX-12R
Kw Crystal or Oscillator
Sc 0
At SMD
AR 
Op 0 0 0
T0 -0.000 -0 1.27 1.27 0 0.254 N V 21 N "Y**"
T1 -0.000 -0 1.27 1.27 0 0.254 N I 21 N "ECS32712512RCTR"
DS -1 -0.6 1 -0.6 0.2 24
DS 1 -0.6 1 0.6 0.2 24
DS 1 0.6 -1 0.6 0.2 24
DS -1 0.6 -1 -0.6 0.2 24
DS -1.6 -1.15 1.6 -1.15 0.1 24
DS 1.6 -1.15 1.6 1.15 0.1 24
DS 1.6 1.15 -1.6 1.15 0.1 24
DS -1.6 1.15 -1.6 -1.15 0.1 24
DS -0.1 -0.6 0.1 -0.6 0.1 21
DS -0.1 0.6 0.1 0.6 0.1 21
DS -1.35 -0.05 -1.35 -0.05 0.1 21
DS -1.35 0.05 -1.35 0.05 0.1 21
DA -1.35 -0 -1.350 -0.05 -1800 0.1 21
DA -1.35 -0 -1.350 0.05 -1800 0.1 21
$PAD
Po -0.700 -0
Sh "1" R 0.800 1.300 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.700 -0
Sh "2" R 0.800 1.300 0 0 0
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE ECS32712512RCTR
$EndLIBRARY
