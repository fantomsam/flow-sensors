<div align="center"> <img src="../ArtWork/Flow_icon.png" width="150"> </div>

# Flow Sensors Project


## Authors
* **Sam Harry Tzavaras** - *Initial work*

## License
The source code of the project is licensed under GPLv3 or later - see the [SW-License](./SW_Design/LICENSE) file for details.<br />
The Hardware design file is licensed under TAPRv1 or later - see the [HW-License](./HW_Design/LICENSE) file for details.
