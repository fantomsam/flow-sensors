<div align="center"> <img src="../ArtWork/Flow_icon.png" width="150"> </div>

# Flow Sensors Software

### Requirements
For compilation of this project the following dependencies required.
* [GCC](https://gcc.gnu.org/) - The GNU Compilers Collection.
* [GNU Make](https://www.gnu.org/software/make/) - GNU make utility.
* [GLib](https://wiki.gnome.org/Projects/GLib) - GNOME core application building blocks libraries.
* [LibGTop](https://developer.gnome.org/libgtop/stable/) - A library to get system specific data.
* [libmodbus](https://www.libmodbus.org/) - A free software library for communication via ModBus protocol.
* [libi2c](https://packages.debian.org/jessie/libi2c-dev) - A library that provide I2C functionality to Userspace.
* [libgpiod](https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git/about/) - Library that give access to GPIOs from Userspace.

### Get the Source
```
$ git clone https://gitlab.com/fantomsam/flow-sensors.git
```

### Compilation of the Project
```
$ make tree
$ make -j$(nproc)
```
### Installation of the Project
```
$ sudo make install
```

### Configuration for the Systemd service
```
$ #Make the nececery modifications on the Unit file of the fls_app service
$ sudo systemctl edit fls_app_system.service --full
$ #Start the systemd service daemon
$ sudo systemctl start fls_app_system.service
$ #--- Optionally If you want to start the fls_app on boot ---
$ sudo systemctl enable fls_app_system.service
```

## Authors
* **Sam Harry Tzavaras** - *Initial work*

## License
The source code of the project is licensed under GPLv3 or later - see the [License](LICENSE) file for details.

