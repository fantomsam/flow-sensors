/*
File: MAX35104_RPI_Driver.c Implementation of MAX35104 Driver for RPi, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#define CONSUMER "fls_app_MAX35104"
#define CHIPNAME "gpiochip0"

#define CNT_INIT 60
#define SAMPLE_CNT_INIT 10
#define T2_PER_T_IDEAL_LIMIT .3

#define INT_LINE_NUM 22
#define WDO_LINE_NUM 23
#define MISO_LINE_NUM 9

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <math.h>
#include <pthread.h>

#include <gpiod.h>

#include <gmodule.h>

#include "../fls_app_types.h"
#include "../Run_AVG_filter.h"
#include "RPi_gpio.h"
#include "MAX35104.h"

/*-- External Global variables--*/
extern pthread_mutex_t MODBus_mem_access;
extern unsigned char running;

/*-- Local Static variables --*/
static MAX35104_config init_conf = {
				//--- Bank_0 ---//
	.Bank0.SW1.One_0   = -1,//Constant bits
	//Configuration bit fields
	.Bank0.SW1.SFREQ   = MAX35104_REG_SWITCHER1_SFREQ_200KHZ,
	.Bank0.SW1.DFREQ   = MAX35104_REG_SWITCHER1_DREQ_200KHZ,
	.Bank0.SW1.VS      = MAX35104_REG_SWITCHER1_VS_23V4,

	.Bank0.SW2.LT_N    = MAX35104_REG_SWITCHER2_LT_N_0V4,
	.Bank0.SW2.LT_S    = MAX35104_REG_SWITCHER2_LT_S_0V4,
	.Bank0.SW2.ST      = MAX35104_REG_SWITCHER2_ST_2050_US,
	.Bank0.SW2.LT_50D  = MAX35104_REG_SWITCHER2_LT_50D_TRIMMED,

	.Bank0.AFE1.SD_EN  = MAX35104_REG_AFE1_SD_EN_DISABLED,

	.Bank0.AFE2.PGA    = MAX35104_REG_AFE2_PGA_29_95DB,
	.Bank0.AFE2.LOWQ   = MAX35104_REG_AFE2_LOWQ_4_2,
	.Bank0.AFE2.BP_BP  = MAX35104_REG_AFE2_BP_BYPASS_DISABLED,
				//--- Bank_1 ---//
	.Bank1.TOF1.PL	     = 20,//Pulses on wave
	.Bank1.TOF1.DPL	     = MAX35104_REG_TOF1_DPL_286KHZ,//Close to center(300kHz) for A300A transducer.
	.Bank1.TOF1.STOP_POL = MAX35104_REG_TOF1_STOP_POL_POS_EDGE,

	.Bank1.TOF2.T2WV	 = 7,//Start measure from this RX pulse
	.Bank1.TOF2.STOP	 = MAX35104_REG_TOF2_STOP_C(4),//Stop pulsing after this RX pulses(plus T2WV).
	.Bank1.TOF2.TOF_CYC	 = MAX35104_REG_TOF2_TOF_CYC_488US,//Delay between Up <-> Down cycles.
	.Bank1.TOF2.TIMOUT	 = MAX35104_REG_TOF2_TIMOUT_4096US,//Set Timeout flag after this time.

	.Bank1.TOF6.C_OFFSETUP  = MAX35104_REG_TOF_C_OFFSET_MV(Vs, 250),
	.Bank1.TOF6.C_OFFSETUPR = MAX35104_REG_TOF_C_OFFSET_MV(Vs, 0),

	.Bank1.TOF7.C_OFFSETDN  = MAX35104_REG_TOF_C_OFFSET_MV(Vs, 250),
	.Bank1.TOF7.C_OFFSETDNR = MAX35104_REG_TOF_C_OFFSET_MV(Vs, 0),

	.Bank1.TOF_DELAY.DLY = MAX35104_REG_TOF_MEASUREMENT_DELAY_DLY_US(600),//Delay after start pulsing.

	.Bank1.CAL_and_CTRL.CAL_PERIOD = 10,//Cycles of 4MHz oscillation that will compared with 32.768KHz.
	//Analog and Digital IO pins.
	.Bank1.CAL_and_CTRL.INT_EN 	   = MAX35104_REG_CALIBRATION_CONTROL_INT_EN_ENABLED,
	.Bank1.CAL_and_CTRL.CLK_S	   = MAX35104_REG_CALIBRATION_CONTROL_CLK_S_CONTINUOUS,
	.Bank1.RTC_CTRL._32K_EN 	   = MAX35104_REG_RTC_32K_EN_ENABLED,
	//AFE related.
	.Bank0.AFE1.AFEOUT 			   = MAX35104_REG_AFE1_AFEOUT_BANDPASS,
	.Bank1.CAL_and_CTRL.CMP_EN     = MAX35104_REG_CALIBRATION_CONTROL_CMP_EN_ENABLED,
	.Bank1.CAL_and_CTRL.CMP_SEL    = MAX35104_REG_CALIBRATION_CONTROL_CMP_SEL_CMP_EN,
};

typedef struct Ultrasonic_sample_data_str{
	double TOF_Up_ms;
	double TOF_Down_ms;
	double TOF_Diff_us;
} Ultrasonic_sample_data;

gpointer Ultrasonic_module_resources_alloc(void *current_sample)
{
	gpointer data = g_slice_new(Ultrasonic_sample_data);

	memcpy(data, current_sample, sizeof(Ultrasonic_sample_data));
	return data;
}

void Ultrasonic_module_samples_buff_summator(gpointer data, gpointer user_data)
{
	Ultrasonic_sample_data *sample_data = (Ultrasonic_sample_data *)data;
	Ultrasonic_sample_data *Ultrasonic_samples_acc = (Ultrasonic_sample_data *)user_data;

	Ultrasonic_samples_acc->TOF_Up_ms   += sample_data->TOF_Up_ms;
	Ultrasonic_samples_acc->TOF_Down_ms += sample_data->TOF_Down_ms;
	Ultrasonic_samples_acc->TOF_Diff_us += sample_data->TOF_Diff_us;
}

void Ultrasonic_module_samples_buff_averagor(void *data, unsigned short window_size)
{
	Ultrasonic_sample_data *Ultrasonic_current_sample = (Ultrasonic_sample_data *)data;

	if(window_size)
	{
		Ultrasonic_current_sample->TOF_Up_ms   /= window_size;
		Ultrasonic_current_sample->TOF_Down_ms /= window_size;
		Ultrasonic_current_sample->TOF_Diff_us /= window_size;
	}
}

void Ultrasonic_mod_sample_buff_free(gpointer data)
{
	if(data)
		g_slice_free(Ultrasonic_sample_data, data);
}

void * Ultrasonic_Hat_thread(void *varg)
{
	struct arg_passer *args = (struct arg_passer*)varg;
	//GPIO related variables.
	struct gpio_chip *RPi_chip;
	struct timespec ts = {0, 100000000};
	struct gpiod_line_event event;
	struct gpiod_chip *chip;
	struct gpiod_line *line;
	//Setup and Environment variables
	control_reg control;
	bool last_Ultrasonic_calc_bit=false;
	float duct_area_sqft, pAir_lbcft, len_inch, cos_a, cAir_fps, path_travel_time_ms;
	float Air_speed_fpm, Vol_flow_cfm, Mass_flow_lbm;
	float Virt_cAir_fps, Virt_Air_temp_F, calc_velocity_press_inWC;
	//MAX35104 related variables.
	unsigned int cnt=0, sample_cnt = SAMPLE_CNT_INIT;
	unsigned short status=0, Ultrasonic_mod_run_AVG_win_size, Ultrasonic_mod_run_AVG_cnt;
	IRQ_STATUS_reg *status_dec = (IRQ_STATUS_reg *)&status;
	float Calibration_value=1, TOF_Up_ms_AVG=0, TOF_Down_ms_AVG=0, TOF_Diff_us_AVG=0;
	float AVG_Air_speed_fpm, AVG_Vol_flow_cfm, AVG_Mass_flow_lbm;
	MAX35104_time_measurements meas_vals;
	//Module_resources struct for rAVG_filter.
	module_resources res = {
		.current_sample = (Ultrasonic_sample_data *)&(meas_vals.TOF_Up_ms),
		.current_sample_size = sizeof(Ultrasonic_sample_data),
		.module_resources_alloc = Ultrasonic_module_resources_alloc,
		.module_resources_free = Ultrasonic_mod_sample_buff_free,
		.module_samples_buff_summator = Ultrasonic_module_samples_buff_summator,
		.module_samples_buff_averagor = Ultrasonic_module_samples_buff_averagor,
		.window_size = &Ultrasonic_mod_run_AVG_win_size
	};

	//Init Ultrasonic Hat related pins.
	if(!(RPi_chip = get_gpio_chip()))
	{
		perror("get_gpio_chip() failed");
		return NULL;
	}
	//Set internal Pull-up on every input pin.
	gpio_set(RPi_chip, INT_LINE_NUM, FUNC_UNSET, DRIVE_UNSET, PULL_UP);
	gpio_set(RPi_chip, WDO_LINE_NUM, FUNC_UNSET, DRIVE_UNSET, PULL_UP);
	gpio_set(RPi_chip, MISO_LINE_NUM, FUNC_UNSET, DRIVE_UNSET, PULL_UP);

	if(!(chip = gpiod_chip_open_by_name(CHIPNAME)))
	{
		perror("gpiod_chip_open_by_name() failed");
		return NULL;
	}
	if(!(line = gpiod_chip_get_line(chip, INT_LINE_NUM)))
	{
		perror("gpiod_chip_get_line() failed");
		gpiod_chip_close(chip);
		return NULL;
	}
	if(gpiod_line_request_falling_edge_events(line, CONSUMER))
	{
		perror("gpiod_line_request_falling_edge_events() failed");
		gpiod_line_release(line);
		gpiod_chip_close(chip);
		return NULL;
	}
	//Initialize MAX35104 for application.
	if(MAX35104_init(&init_conf))
	{
		fprintf(stderr, "MAX35104_init() failed!!!\n");
		gpiod_line_release(line);
		gpiod_chip_close(chip);
		return NULL;
	}
	//Main thread's Loop.
	res.samples_buff = g_queue_new();
	while(running)
	{
		//Get setup and environment variables
		pthread_mutex_lock(&MODBus_mem_access);
			control = args->mb_RDWR->control;
			duct_area_sqft = args->mb_RDWR->config.Duct_area_sqft;
			len_inch = args->mb_RDWR->config.Ultrasonic_sensors_len_inch;
			cos_a = args->mb_RDWR->config.Ultrasonic_sensors_cos_a;
			Ultrasonic_mod_run_AVG_win_size = args->mb_RDWR->config.Ultrasonic_mod_run_AVG_win_size;
			pAir_lbcft = args->mb_RD->pAir_lbcft;
			cAir_fps = args->mb_RD->cAir_fps;
		pthread_mutex_unlock(&MODBus_mem_access);
		//Wait IRQ from MAX35104.
		if(!gpiod_line_event_wait(line, &ts))
		{
			if(control.bits.Global_on_off && control.bits.Ultrasonic_on_off)
			{
				if(!cnt)
				{
					MAX35104_filter_autocalibration(&init_conf);
					MAX35104_send_opcode(MAX35104_OPCODE_CALIBRATE);
					cnt = CNT_INIT;
				}
				else
				{
					MAX35104_send_opcode(MAX35104_OPCODE_TOF_DIFF);
					cnt--;
				}
			}
			else
			{
				MAX35104_get_status(status_dec);
				if(!g_queue_is_empty(res.samples_buff))
					g_queue_clear_full(res.samples_buff, Ultrasonic_mod_sample_buff_free);
				pthread_mutex_lock(&MODBus_mem_access);
					args->mb_RD->Ultrasonic_mod_run_AVG_cnt = 0;
					args->mb_RD->status_MAX35104 = status;
					args->mb_RD->TOF_Up_ms = NAN;
					args->mb_RD->TOF_Down_ms = NAN;
					args->mb_RD->TOF_Diff_us = NAN;
					args->mb_RD->Air_speed_fpm = NAN;
					args->mb_RD->Vol_flow_cfm = NAN;
					args->mb_RD->Mass_flow_lbm = NAN;
					args->mb_RD->t1_per_t2_Up = NAN;
					args->mb_RD->t2_per_t_ideal_Up = NAN;
					args->mb_RD->t1_per_t2_Down = NAN;
					args->mb_RD->t2_per_t_ideal_Down = NAN;
					args->mb_RD->calc_velocity_press_inWC = NAN;
					args->mb_RD->virt_Air_temp_F = NAN;
					args->mb_RD->AVG_Air_speed_fpm = NAN;
					args->mb_RD->AVG_Vol_flow_cfm = NAN;
					args->mb_RD->AVG_Mass_flow_lbm = NAN;
				pthread_mutex_unlock(&MODBus_mem_access);
				TOF_Up_ms_AVG = 0;
				TOF_Down_ms_AVG = 0;
				TOF_Diff_us_AVG = 0;
				sample_cnt = SAMPLE_CNT_INIT;
				cnt = 0;
			}
			continue;
		}
		gpiod_line_event_read(line, &event);
		//Get MAX35104's status register and load it to dedicated MODBus space.
		MAX35104_get_status(status_dec);
		pthread_mutex_lock(&MODBus_mem_access);
			args->mb_RD->status_MAX35104 = status;
		pthread_mutex_unlock(&MODBus_mem_access);
		if(status_dec->CAL)
		{
			MAX35104_get_calibration(&Calibration_value);
			MAX35104_send_opcode(MAX35104_OPCODE_TOF_DIFF);
		}
		if(status_dec->TOF && !status_dec->TimeOut)//Get new measurements from MAX35104.
		{
			if(!MAX35104_get_meas(&meas_vals))
			{
				meas_vals.TOF_Up_ms = round(Calibration_value*meas_vals.TOF_Up_ms*1e4)/1e4;
				meas_vals.TOF_Down_ms = round(Calibration_value*meas_vals.TOF_Down_ms*1e4)/1e4;
				meas_vals.TOF_Diff_us = round(Calibration_value*meas_vals.TOF_Diff_us*1e1)/1e1;
				if(sample_cnt)
				{
					TOF_Up_ms_AVG += meas_vals.TOF_Up_ms;
					TOF_Down_ms_AVG += meas_vals.TOF_Down_ms;
					TOF_Diff_us_AVG += meas_vals.TOF_Diff_us;
					sample_cnt--;
				}
				else
				{
					TOF_Up_ms_AVG /= SAMPLE_CNT_INIT;
					TOF_Down_ms_AVG /= SAMPLE_CNT_INIT;
					TOF_Diff_us_AVG /= SAMPLE_CNT_INIT;
					if(control.bits.Ultrasonic_calc && !last_Ultrasonic_calc_bit)
					{
						if(!isnanf(cAir_fps))
						{
							path_travel_time_ms = (TOF_Up_ms_AVG + TOF_Down_ms_AVG)/2.0;
							len_inch = roundf(cAir_fps*path_travel_time_ms*12.0)/1e3;
							pthread_mutex_lock(&MODBus_mem_access);
								args->mb_RDWR->config.Ultrasonic_sensors_len_inch = len_inch;
								args->mb_RD->config_status.bits.config_not_saved = true;
								args->mb_RD->config_status.bits.Ultrasonic_calc_Error = false;
							pthread_mutex_unlock(&MODBus_mem_access);
						}
						else
						{
							pthread_mutex_lock(&MODBus_mem_access);
								args->mb_RD->config_status.bits.Ultrasonic_calc_Error = true;
							pthread_mutex_unlock(&MODBus_mem_access);
						}
					}
					Virt_cAir_fps = len_inch*(TOF_Up_ms_AVG+TOF_Down_ms_AVG)*1e3/(24.0*(TOF_Up_ms_AVG*TOF_Down_ms_AVG));
					Virt_Air_temp_F = powf(Virt_cAir_fps/49.03, 2.0)-460.0; //Calculate virtual air temperature in °F.
					AVG_Air_speed_fpm = 2.5*(len_inch/cos_a)*(TOF_Diff_us_AVG/(TOF_Up_ms_AVG*TOF_Down_ms_AVG));
					AVG_Vol_flow_cfm = AVG_Air_speed_fpm*duct_area_sqft;
					AVG_Mass_flow_lbm = AVG_Vol_flow_cfm*pAir_lbcft;
					calc_velocity_press_inWC = copysignf(roundf(powf(AVG_Air_speed_fpm/1096.7, 2.0)*pAir_lbcft*1e3)/1e3, AVG_Air_speed_fpm);
					pthread_mutex_lock(&MODBus_mem_access);
						//Virtual Temp(°F), calc velocity press(inWC) and average values.
						args->mb_RD->virt_Air_temp_F = Virt_Air_temp_F;
						args->mb_RD->calc_velocity_press_inWC = calc_velocity_press_inWC;
						args->mb_RD->AVG_Air_speed_fpm = AVG_Air_speed_fpm;
						args->mb_RD->AVG_Vol_flow_cfm = AVG_Vol_flow_cfm;
						args->mb_RD->AVG_Mass_flow_lbm = AVG_Mass_flow_lbm;
					pthread_mutex_unlock(&MODBus_mem_access);
					TOF_Up_ms_AVG = 0;
					TOF_Down_ms_AVG = 0;
					TOF_Diff_us_AVG = 0;
					last_Ultrasonic_calc_bit = control.bits.Ultrasonic_calc;
					sample_cnt = SAMPLE_CNT_INIT;
				}
				Ultrasonic_mod_run_AVG_cnt = rAVG_filter(&res);
				Air_speed_fpm = 2.5*(len_inch/cos_a)*(meas_vals.TOF_Diff_us/(meas_vals.TOF_Up_ms*meas_vals.TOF_Down_ms));
				Vol_flow_cfm = Air_speed_fpm*duct_area_sqft;
				Mass_flow_lbm = pAir_lbcft*Vol_flow_cfm;
				pthread_mutex_lock(&MODBus_mem_access);
					args->mb_RD->Ultrasonic_mod_run_AVG_cnt = Ultrasonic_mod_run_AVG_cnt;
					//Acoustic Wave analysis.
					args->mb_RD->t1_per_t2_Up = meas_vals.wave_ratios.t1_per_t2_Up;
					args->mb_RD->t2_per_t_ideal_Up = meas_vals.wave_ratios.t2_per_t_ideal_Up;
					args->mb_RD->t1_per_t2_Down = meas_vals.wave_ratios.t1_per_t2_Down;
					args->mb_RD->t2_per_t_ideal_Down = meas_vals.wave_ratios.t2_per_t_ideal_Down;
					//TOF Measurements.
					args->mb_RD->TOF_Up_ms = meas_vals.TOF_Up_ms;
					args->mb_RD->TOF_Down_ms = meas_vals.TOF_Down_ms;
					args->mb_RD->TOF_Diff_us = meas_vals.TOF_Diff_us;
					//Speed and Flows.
					args->mb_RD->Air_speed_fpm = Air_speed_fpm;
					args->mb_RD->Vol_flow_cfm = Vol_flow_cfm;
					args->mb_RD->Mass_flow_lbm = Mass_flow_lbm;
				pthread_mutex_unlock(&MODBus_mem_access);
			}
			else
				fprintf(stderr, "MAX35104_get_meas() failed!!!\n");
		}
	}
	gpiod_line_release(line);
	gpiod_chip_close(chip);
	g_queue_free_full(res.samples_buff, Ultrasonic_mod_sample_buff_free);
	return NULL;
}
