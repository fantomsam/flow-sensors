/*
File: MAX35104.c Declaration of functions for MAX35104, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MAX35104_DEF_H
#define MAX35104_DEF_H

#include "MAX35104_defs.h"

#define Vs 3.3

#define MAX35104_BANK0_SADDR   	MAX35104_REG_SWITCHER1
#define MAX35104_BANK1_SADDR   	MAX35104_REG_TOF1
#define MAX35104_MEAS_REG_SADDR 	MAX35104_REG_WVRUP
#define MAX35104_MEAS_CAL_SADDR  MAX35104_REG_CALIBRATIONINT

#define MAX35104_BANK0_SIZE 	sizeof(Config_regs_Bank0)
#define MAX35104_BANK1_SIZE 	sizeof(Config_regs_Bank1)

#pragma pack(push, 1)//use pragma pack() to pack the following structs to 1 byte.
typedef struct REG_Switch_1 {
	unsigned VS 	 : 4;
	unsigned One_0   : 2;
	unsigned DFREQ 	 : 2;
	unsigned RES_0   : 4;
	unsigned Zero_0	 : 1;
	unsigned HREG_D  : 1;
	unsigned SFREQ   : 2;
} Switch_1_reg;

typedef struct REG_Switch_2 {
	unsigned PECHO 	 : 1;
	unsigned Zero_0  : 2;
	unsigned LT_50D  : 1;
	unsigned ST 	 : 4;
	unsigned LT_S 	 : 4;
	unsigned LT_N 	 : 4;
} Switch_2_reg;

typedef struct REG_AFE1 {
	unsigned WB      : 7;
	unsigned Zero_0  : 1;
	unsigned AFEOUT  : 2;
	unsigned SD_EN   : 1;
	unsigned Zero_1  : 4;
	unsigned AFE_BP  : 1;
} AFE1_reg;

typedef struct REG_AFE2 {
	unsigned BP_BP    : 1;
	unsigned Zero_0   : 1;
	unsigned LOWQ     : 2;
	unsigned PGA      : 4;
	unsigned F0		  : 7;
	unsigned _4M_BP   : 1;
} AFE2_reg;

typedef struct REG_TOF1 {
	unsigned RES_0   : 3;
	unsigned STOP_POL: 1;
	unsigned DPL	 : 4;
	unsigned PL		 : 8;
} TOF1_reg;

typedef struct REG_TOF2 {
	unsigned TIMOUT  : 3;
	unsigned RES_0   : 1;
	unsigned TOF_CYC : 3;
	unsigned T2WV	 : 6;
	unsigned STOP    : 3;
} TOF2_reg;

typedef struct REG_TOF3 {
	unsigned HIT2WV  : 6;
	unsigned RES_1   : 2;
	unsigned HIT1WV	 : 6;
	unsigned RES_0   : 2;
} TOF3_reg;

typedef struct REG_TOF4 {
	unsigned HIT4WV  : 6;
	unsigned RES_1   : 2;
	unsigned HIT3WV	 : 6;
	unsigned RES_0   : 2;
} TOF4_reg;

typedef struct REG_TOF5 {
	unsigned HIT6WV  : 6;
	unsigned RES_1   : 2;
	unsigned HIT5WV	 : 6;
	unsigned RES_0   : 2;
} TOF5_reg;

typedef struct REG_TOF6 {
	unsigned C_OFFSETUP  : 7;
	unsigned RES_0       : 1;
	char	 C_OFFSETUPR : 8;
} TOF6_reg;

typedef struct REG_TOF7 {
	unsigned C_OFFSETDN  : 7;
	unsigned RES_0       : 1;
	char	 C_OFFSETDNR : 8;
} TOF7_reg;

typedef struct REG_EVENT_1 {
	unsigned RES_0   : 1;
	unsigned TMF 	 : 6;
	unsigned TDM 	 : 5;
	unsigned TDF	 : 4;
} EVENT_1_reg;

typedef struct REG_EVENT_2 {
	unsigned PORTCYC : 2;
	unsigned PRECYC  : 3;
	unsigned RES_0   : 2;
	unsigned CAL_CFG : 3;
	unsigned CAL_USE : 1;
	unsigned TMM	 : 5;
} EVENT_2_reg;

typedef struct REG_TOF_DELAY {
	unsigned short DLY;
} TOF_DELAY_reg;

typedef struct REG_CAL_and_CTRL {
	unsigned CAL_PERIOD	: 4;
	unsigned CLK_S		: 3;
	unsigned CONT_INT   : 1;
	unsigned ET_CONT  	: 1;
	unsigned INT_EN  	: 1;
	unsigned CMP_SEL  	: 1;
	unsigned CMP_EN   	: 1;
	unsigned RES_0  	: 4;
} CAL_and_CTRL_reg;

typedef struct REG_RTC_CTRL {
	unsigned WD_EN	  : 1;
	unsigned WF       : 1;
	unsigned AM       : 2;
	unsigned EOSC     : 1;
	unsigned _32K_EN  : 1;
	unsigned _32K_BP  : 1;
	unsigned RES_0	  : 9;
} RTC_CTRL_reg;

typedef struct REG_IRQ_STATUS {
	unsigned RES_3     : 2;
	unsigned POR       : 1;
	unsigned RES_2     : 1;
	unsigned CSWI      : 1;
	unsigned HALT      : 1;
	unsigned CAL  	   : 1;
	unsigned RES_1     : 1;
	unsigned TEMP_EVTMG: 1;
	unsigned TOF_EVTMG : 1;
	unsigned LDO       : 1;
	unsigned TE        : 1;
	unsigned TOF       : 1;
	unsigned RES_0     : 1;
	unsigned AF   	   : 1;
	unsigned TimeOut   : 1;
} IRQ_STATUS_reg;

typedef struct REG_CTRL {
	unsigned HWR  	   : 4;
	unsigned RES_1     : 4;
	unsigned CSWA      : 1;
	unsigned AFA       : 1;
	unsigned RES_0     : 6;
} CTRL_reg;

typedef struct Config_regs_Bank1_struct{
	Switch_1_reg	SW1;
	Switch_2_reg	SW2;
	AFE1_reg		AFE1;
	AFE2_reg		AFE2;
} Config_regs_Bank0;

typedef struct Config_regs_Bank2_struct{
	TOF1_reg 			TOF1;
	TOF2_reg			TOF2;
	TOF3_reg			TOF3;
	TOF4_reg			TOF4;
	TOF5_reg			TOF5;
	TOF6_reg			TOF6;
	TOF7_reg			TOF7;
	EVENT_1_reg			EVENT_1;
	EVENT_2_reg			EVENT_2;
	TOF_DELAY_reg		TOF_DELAY;
	CAL_and_CTRL_reg	CAL_and_CTRL;
	RTC_CTRL_reg		RTC_CTRL;
} Config_regs_Bank1;

typedef struct MAX35104_config_struct{
	Config_regs_Bank0 Bank0;
	Config_regs_Bank1 Bank1;
} MAX35104_config;

typedef struct Fix_point_num_struct{
	unsigned short Int;
	unsigned short Frac;
} FPnum_reg;

typedef struct WVR_reg_struct {
	unsigned char t1_per_t2;
	unsigned char t2_per_t_ideal;
} WVR_reg;

typedef struct MAX35104_meas_struct{
	WVR_reg WVRUP;
	FPnum_reg Hit_up[6];
	FPnum_reg Hit_up_avg;
	WVR_reg WVRDN;
	FPnum_reg Hit_dn[6];
	FPnum_reg Hit_dn_avg;
	FPnum_reg TOF_DIFF;
} MAX35104_meas;
#pragma pack(pop)//Disable packing

typedef struct MAX35104_time_measurements_struct{
	struct wave_ratios_struct{
		float t1_per_t2_Up;
		float t1_per_t2_Down;
		float t2_per_t_ideal_Up;
		float t2_per_t_ideal_Down;
	} wave_ratios;
	double TOF_Up_ms;
	double TOF_Down_ms;
	double TOF_Diff_us;
} MAX35104_time_measurements;

//Interface functions for MAX35104.
int MAX35104_init(MAX35104_config *config);
int MAX35104_send_opcode(unsigned char opcode);
int MAX35104_filter_autocalibration(MAX35104_config *config);
int MAX35104_get_status(IRQ_STATUS_reg *status);
int MAX35104_get_meas(MAX35104_time_measurements *time_meas);
int MAX35104_get_calibration(float *cal_val);

/*
int MAX35104_read_reg(unsigned char addr, unsigned short *value);
int MAX35104_write_reg(unsigned char addr, const unsigned short value);

int MAX35104_read_regs(unsigned char addr, void *data, unsigned char len);
int MAX35104_write_regs(unsigned char addr, const void *data, unsigned char len);
*/
#endif //MAX35104_DEF_H
