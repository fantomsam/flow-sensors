/*
File: RPi_gpio.c Declaretion of functions Related to Raspberry Pi's GPIO, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef RPI_GPIO_H
#define RPI_GPIO_H

#include <stdint.h>

#define GPIO_BASE_OFFSET 0x00200000

#define DRIVE_UNSET -1
#define DRIVE_LOW    0
#define DRIVE_HIGH   1

#define FUNC_UNSET  -1
#define FUNC_IP      0
#define FUNC_OP      1
#define FUNC_ALT(x)  (2 + (x))
#define FUNC_A0      FUNC_ALT(0)
#define FUNC_A1      FUNC_ALT(1)
#define FUNC_A2      FUNC_ALT(2)
#define FUNC_A3      FUNC_ALT(3)
#define FUNC_A4      FUNC_ALT(4)
#define FUNC_A5      FUNC_ALT(5)

#define PULL_UNSET  -1
#define PULL_NONE    0
#define PULL_DOWN    1
#define PULL_UP      2

struct gpio_chip
{
    const char *name;
    uint32_t reg_base;
    uint32_t reg_size;
    unsigned int gpio_count;
    unsigned int fsel_count;
    const char *info_header;
    const char **alt_names;
    const int *default_pulls;

    int (*get_level)(struct gpio_chip *chip, unsigned int gpio);
    int (*get_fsel)(struct gpio_chip *chip, unsigned int gpio);
    int (*get_pull)(struct gpio_chip *chip, unsigned int gpio);
    int (*set_level)(struct gpio_chip *chip, unsigned int gpio, int level);
    int (*set_fsel)(struct gpio_chip *chip, unsigned int gpio, int fsel);
    int (*set_pull)(struct gpio_chip *chip, unsigned int gpio, int pull);
    int (*next_reg)(int reg);

    volatile uint32_t *base;
};

struct gpio_chip *get_gpio_chip(void);
const char *gpio_fsel_to_namestr(struct gpio_chip *chip, unsigned int gpio, int fsel);
const char *gpio_get_str(struct gpio_chip *chip, unsigned int gpio);
int gpio_set(struct gpio_chip *chip, unsigned int gpio, int fsparam, int drive, int pull);

#endif //RPI_GPIO_H
