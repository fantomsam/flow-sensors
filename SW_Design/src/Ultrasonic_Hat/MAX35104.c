/*
File: MAX35104.c Implementation of functions for MAX35104, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#define DEV_NAME "/dev/spidev0.0"

#include <stdio.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>

#include <sys/ioctl.h>
#include <linux/ioctl.h>
#include <sys/stat.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include "MAX35104.h"
#include "MAX35104_defs.h"

int max35104_spi_xfer(void *tx, const void *rx, unsigned char count)
{
	int fd, ret = 0;
	const unsigned char mode = SPI_MODE_1;
	struct spi_ioc_transfer buff = {
		.tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.delay_usecs = 10,
		.len = count,
		.speed_hz = 15600000,
	};

	if((fd = open(DEV_NAME, O_RDWR)) < 0)
	{
		perror("open() failed");
		return -1;
	}
	if(!ioctl(fd, SPI_IOC_WR_MODE, &mode))
		ioctl(fd, SPI_IOC_MESSAGE(1), &buff);
	else
		ret = -1;
	close(fd);
	return ret;
}

int MAX35104_read_reg(unsigned char addr, unsigned short *value)
{
	unsigned char tx[3]={MAX35104_OPCODE_READ_REG(addr)}, rx[3];

	if(!value)
		return -1;
	if(max35104_spi_xfer(tx, rx, sizeof(tx)))
		return -1;
	memcpy(value, rx+1, sizeof(unsigned short));
	#if defined(__BYTE_ORDER__)&&(__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
	*value = be16toh(*value);
	#endif
	return 0;
}

int MAX35104_write_reg(unsigned char addr, const unsigned short value)
{
	unsigned char tx[3]={MAX35104_OPCODE_WRITE_REG(addr)};

	memcpy(tx+1, (unsigned char*)&value, sizeof(unsigned short));
	#if defined(__BYTE_ORDER__)&&(__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
	*((unsigned short *)(tx+1)) = htobe16(*((unsigned short *)(tx+1)));
	#endif
	return max35104_spi_xfer(tx, NULL, sizeof(tx));
}

int MAX35104_read_regs(unsigned char addr, void *data, unsigned char len)
{
	int ret=0;
	unsigned char *tx, *rx;

	if(!data)
		return -1;
	if(!((tx = calloc(len+1, 1))&&(rx = malloc(len+1))))
	{
		fprintf(stderr,"Memory Error!!!\n");
		exit(1);
	}
	tx[0] = MAX35104_OPCODE_READ_REG(addr);
	if(!(ret = max35104_spi_xfer(tx, rx, len+1)))
	{
		memcpy(data, rx+1, len);
		#if defined(__BYTE_ORDER__)&&(__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
		unsigned short *buff = (unsigned short *)data;
		for(int i=0; i<len/sizeof(unsigned short); i++)
			buff[i] = be16toh(buff[i]);
		#endif
	}
	free(tx);
	free(rx);
	return ret;
}

int MAX35104_write_regs(unsigned char addr, const void *data, unsigned char len)
{
	int ret=0;
	unsigned char *tx;

	if(!data)
		return -1;
	if(!(tx = calloc(len+1, 1)))
	{
		fprintf(stderr,"Memory Error!!!\n");
		exit(1);
	}
	tx[0] = MAX35104_OPCODE_WRITE_REG(addr);
	memcpy(tx+1, data, len);
	#if defined(__BYTE_ORDER__)&&(__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
	unsigned short *buff = (unsigned short *)(tx+1);
	for(int i=0; i<len/sizeof(unsigned short); i++)
		buff[i] = htobe16(buff[i]);
	#endif
	ret = max35104_spi_xfer(tx, NULL, len+1);
	free(tx);
	return ret;
}

int MAX35104_send_opcode(unsigned char opcode)
{
	return max35104_spi_xfer(&opcode, NULL, 1);
}

int MAX35104_get_status(IRQ_STATUS_reg *status)
{
	return MAX35104_read_reg(MAX35104_REG_INTERRUPT_STATUS, (unsigned short *)status);
}

int MAX35104_filter_autocalibration(MAX35104_config *config)
{
	unsigned short AFE2_reg_val;
	AFE2_reg *AFE2_reg_dec = (AFE2_reg *)&AFE2_reg_val;

	if(!config)
		return -1;
	config->Bank0.AFE2.F0=0;
	*AFE2_reg_dec = config->Bank0.AFE2;
	if(MAX35104_write_reg(MAX35104_REG_AFE2, AFE2_reg_val))
		return -1;
	if(MAX35104_send_opcode(MAX35104_OPCODE_BANDPASS_CAL))
		return -1;
	usleep(10000);
	if(MAX35104_read_reg(MAX35104_REG_AFE2, &AFE2_reg_val))
		return -1;
	config->Bank0.AFE2.F0 = AFE2_reg_dec->F0;
	return 0;
}

static double MAX35104_Time_fixed_point_to_double_ms(const FPnum_reg FP_val)
{
	return (double)((FP_val.Int/4000.0)+(FP_val.Frac/(65536.0*4000.0)));
}

static double MAX35104_TOF_fixed_point_to_double_us(const FPnum_reg FP_val)
{
	return (double)((((short)FP_val.Int)/4.0)+(FP_val.Frac/(65536.0*4.0)));
}

static void MAX35104_WVRs_to_MAX35104_time_meas(MAX35104_meas *meas_regs, MAX35104_time_measurements *meas_vals)
{
	struct IEEE754_dec_struct{
		unsigned mantissa: 23;
		unsigned exponent: 8;
		unsigned sign: 1;
	}__attribute__ ((packed)) *IEEE754_dec;
	unsigned char  WVR_val;

	if(!meas_regs || !meas_vals)
		return;
	for(int i=0; i<4; i++)
	{
		switch(i)
		{
			case 0:
				IEEE754_dec = (struct IEEE754_dec_struct *)&(meas_vals->wave_ratios.t1_per_t2_Up);
				WVR_val = meas_regs->WVRUP.t1_per_t2;
				break;
			case 1:
				IEEE754_dec = (struct IEEE754_dec_struct *)&(meas_vals->wave_ratios.t1_per_t2_Down);
				WVR_val = meas_regs->WVRDN.t1_per_t2;
				break;
			case 2:
				IEEE754_dec = (struct IEEE754_dec_struct *)&(meas_vals->wave_ratios.t2_per_t_ideal_Up);
				WVR_val = meas_regs->WVRUP.t2_per_t_ideal;
				break;
			case 3:
				IEEE754_dec = (struct IEEE754_dec_struct *)&(meas_vals->wave_ratios.t2_per_t_ideal_Down);
				WVR_val = meas_regs->WVRDN.t2_per_t_ideal;
				break;
		}
		IEEE754_dec->exponent = 0x7F;
		while(!(WVR_val & 0x80))
		{
			WVR_val<<=1;
			IEEE754_dec->exponent--;
		}
		IEEE754_dec->mantissa = WVR_val<<16;
		IEEE754_dec->sign = 0;
	}
}

int MAX35104_get_meas(MAX35104_time_measurements *time_meas)
{
	MAX35104_meas meas_vals;

	if(!time_meas)
		return -1;
	if(MAX35104_read_regs(MAX35104_MEAS_REG_SADDR, &meas_vals, sizeof(MAX35104_meas)))
		return -1;
	MAX35104_WVRs_to_MAX35104_time_meas(&meas_vals, time_meas);
	time_meas->TOF_Up_ms = MAX35104_Time_fixed_point_to_double_ms(meas_vals.Hit_up_avg);
	time_meas->TOF_Down_ms = MAX35104_Time_fixed_point_to_double_ms(meas_vals.Hit_dn_avg);
	time_meas->TOF_Diff_us = MAX35104_TOF_fixed_point_to_double_us(meas_vals.TOF_DIFF);
	return 0;
}

int MAX35104_get_calibration(float *calibration_value)
{
	FPnum_reg cal_val;
	double cal_val_t;

	if(MAX35104_read_regs(MAX35104_MEAS_CAL_SADDR, &cal_val, sizeof(FPnum_reg)))
		return -1;
	cal_val_t = MAX35104_Time_fixed_point_to_double_ms(cal_val);
	cal_val_t = 122.0703125/(cal_val_t*4000.0);
	*calibration_value = cal_val_t;
	return 0;
}

int MAX35104_init(MAX35104_config *config)
{
	unsigned char cnt = -1;
	unsigned short status = 0, wback = 0;
	IRQ_STATUS_reg *status_dec = (IRQ_STATUS_reg *)&status;
	AFE1_reg *AFE1 = (AFE1_reg *)&wback;

	if(!config)
		return -1;
	//Reset MAX35104.
	MAX35104_send_opcode(MAX35104_OPCODE_RESET);
	do{
		if(MAX35104_get_status(status_dec))
			return -1;
		cnt--;
	}while(status&&cnt);
	if(!cnt)
		return -1;
	//Get write back value from MAX35104.
	MAX35104_read_reg(MAX35104_REG_AFE1, &wback);
	config->Bank0.AFE1.WB = AFE1->WB;
	//Send application's configuration to MAX35104
	MAX35104_write_regs(MAX35104_BANK0_SADDR, &(config->Bank0), MAX35104_BANK0_SIZE);
	MAX35104_write_regs(MAX35104_BANK1_SADDR, &(config->Bank1), MAX35104_BANK1_SIZE);
	return 0;
}
