/*
File: MAX35104_RPI_Driver.h Declaration of function for MAX35104 Driver for RPi, Part of Flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef ULTRASONIC_HAT_H
#define ULTRASONIC_HAT_H

void * Ultrasonic_Hat_thread(void *varg);

#endif //ULTRASONIC_HAT_H
