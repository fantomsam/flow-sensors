/*
Program: fls_app. Main software of the flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#define VERSION "1.5" /*Release Version*/
#define n_threads 2

#define config_dir_path "/var/tmp/fls_app/"
#define config_file_path "/var/tmp/fls_app/fls_app_config"

#define DEFAULT_TCP_PORT 502
#define DEFAULT_UNIT_ID 255
#define NB_CONNECTION 5

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <getopt.h>
#include <time.h>
#include <math.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <sys/time.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <modbus.h>

#include "fls_app_types.h"

#include "Ultrasonic_Hat/Ultrasonic_Hat.h"
#include "meas_net/meas_net.h"

/*--- Global variables ---*/
pthread_mutex_t MODBus_mem_access = PTHREAD_MUTEX_INITIALIZER;
volatile unsigned char running = true;

/*--- Local Functions ---*/
void print_usage(char *prog_name);//print the Usage manual
int fls_app_config_file(fls_app_config *app_conf, const char *mode);//Function to load/store app's config.
unsigned int get_config_start_addr(mb_RDWR_regs *RDWR_regs);//Function that calculate the relative first position of the application's configuration.
bool config_check(MODBus_TCP_query *query, unsigned short config_start_addr, bool debug);//Function that check if the app's configuration have been changed.
static void stopHandler(int sign)
{
    if(sign==SIGINT)
		fprintf(stderr,"Received ctrl-c\n");
	running = false;
}

int main(int argc, char *argv[])
{
	DIR *dir;
	pthread_t thread_ids[n_threads] = {0};
	uint8_t query[MODBUS_TCP_MAX_ADU_LENGTH];
	unsigned int config_start_addr;
	bool last_Reset_CNTs_bit=0, last_Restore_config_bit=0, last_Save_config_bit=0;
	MODBus_TCP_query *mb_query = (MODBus_TCP_query*)query;
	modbus_t *ctx = NULL;
	modbus_mapping_t *MODBus_mem = NULL;
	struct sockaddr_in clientaddr;
	socklen_t addrlen = sizeof(clientaddr);
	int newfd, master_socket, rc, fdmax, server_socket = -1;
	fd_set refset, rdset;
	struct arg_passer arg_pass;

	//Get options
	int c, tcp_port=DEFAULT_TCP_PORT, Unit_ID = DEFAULT_UNIT_ID;
	bool verbose=false, debug=false;
	while ((c = getopt(argc, argv, "hVvdp:u:")) != -1)
	{
		switch (c)
		{
			case 'h'://Help
				print_usage(argv[0]);
				exit(EXIT_SUCCESS);
			case 'V'://Version
				printf("fls_app:v"VERSION"\n"
					   "libMODBus:v"LIBMODBUS_VERSION_STRING"\n");
				exit(EXIT_SUCCESS);
			case 'v'://Verbose mode
				verbose = true;
				break;
			case 'd'://debug mode
				debug = true;
				break;
			case 'p'://TCP-Port
				if(!(tcp_port = atoi(optarg)) || (tcp_port<=0 || tcp_port>65535))
				{
					printf("Argument for TCP-Port:(%d) is Invalid!!!\n\n", tcp_port);
					print_usage(argv[0]);
					exit(EXIT_SUCCESS);
				}
				break;
			case 'u'://Unit-ID
				Unit_ID = atoi(optarg);
				if(Unit_ID<=0 || Unit_ID>247)
				{
					printf("Argument for Unit_ID:(%d) is out of range (1..247)!!!\n\n", Unit_ID);
					print_usage(argv[0]);
					exit(EXIT_SUCCESS);
				}
				break;
			case '?':
				print_usage(argv[0]);
				exit(EXIT_FAILURE);
		}
	}

	//Check the existence of configuration file directory, create it if does not exist.
	dir = opendir(config_dir_path);
	if(dir)
		closedir(dir);
	else if(mkdir(config_dir_path, S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH))
	{
		perror("mkdir() failed!!!");
		return EXIT_FAILURE;
	}

    if(!(ctx = modbus_new_tcp("0.0.0.0", tcp_port)))
	{
		fprintf(stderr, "Unable to allocate libmodbus context: %s\n", modbus_strerror(errno));
		return EXIT_FAILURE;
	}
	if(!(MODBus_mem = modbus_mapping_new(0, 0, mb_mem_size(mb_RDWR_regs), mb_mem_size(mb_RD_regs))))
	{
		fprintf(stderr, "Failed to allocate the mapping: %s\n", modbus_strerror(errno));
		modbus_free(ctx);
		return EXIT_FAILURE;
	}

    if(verbose)
		printf("Application MODBus Server Listen@TCP:%d%s on Unit-ID:%d%s\n", tcp_port, tcp_port==DEFAULT_TCP_PORT?" (Default)":"",
																			   Unit_ID,  Unit_ID==DEFAULT_UNIT_ID ?" (Default)":"");
	if((server_socket = modbus_tcp_listen(ctx, NB_CONNECTION)) == -1)
	{
        fprintf(stderr, "Unable to listen TCP connection @ Port:%d\n", tcp_port);
        modbus_mapping_free(MODBus_mem);
		modbus_free(ctx);
        return EXIT_FAILURE;
    }

	if(modbus_set_debug(ctx, debug))//libModBus debug
	{
		perror("modbus_set_debug() Failed!!!");
		modbus_mapping_free(MODBus_mem);
		modbus_free(ctx);
        return EXIT_FAILURE;
	}

	arg_pass.mb_RD = (mb_RD_regs *)(MODBus_mem->tab_input_registers);
	arg_pass.mb_RDWR = (mb_RDWR_regs *)(MODBus_mem->tab_registers);
	config_start_addr = get_config_start_addr(arg_pass.mb_RDWR);

	//Get and load stored configuration.
	if(fls_app_config_file(&(arg_pass.mb_RDWR->config), "r"))
		arg_pass.mb_RDWR->config = default_app_config;

	//Install stopHandler as the signal handler for SIGINT and SIGTERM signals.
	signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

	pthread_create(&thread_ids[0], NULL, Ultrasonic_Hat_thread, &arg_pass);//Start thread for Ultrasonic Hat(MAX35104)
	pthread_create(&thread_ids[1], NULL, meas_net_thread, &arg_pass);//Start thread for devices at meas_net

	FD_ZERO(&refset);//Clear the reference set of socket
	FD_SET(server_socket, &refset);//Add the server socket
	fdmax = server_socket;//Keep track of the max file descriptor

    while(running)
	{
        rdset = refset;
        if(select(fdmax+1, &rdset, NULL, NULL, NULL) < 0)
			continue;
       	//Run through the existing connections looking for data to be read
        for(master_socket = 0; master_socket <= fdmax && running; master_socket++)
		{
			if(!FD_ISSET(master_socket, &rdset))
				continue;
            if(master_socket == server_socket)// A client is asking a new connection
			{   //Handle new connections
                memset(&clientaddr, 0, sizeof(clientaddr));
                newfd = accept(server_socket, (struct sockaddr *)&clientaddr, &addrlen);
                if(newfd == -1)
                    perror("Server accept() error");
				else
				{
                    FD_SET(newfd, &refset);
                    if(newfd > fdmax)//Keep track of the maximum
						fdmax = newfd;
					if(verbose)
		                printf("New connection from %s:%d on socket %d\n", inet_ntoa(clientaddr.sin_addr),
																		   clientaddr.sin_port, newfd);
                }
            }
			else
			{
                modbus_set_socket(ctx, master_socket);
                rc = modbus_receive(ctx, query);
                if(rc>0)
                {
                	if(mb_query->unit_ID == Unit_ID)
					{
						pthread_mutex_lock(&MODBus_mem_access);
							arg_pass.mb_RD->poll_index++;
						pthread_mutex_unlock(&MODBus_mem_access);
						if(!mb_accepted_funcs(mb_query->func_code))
		            	{
		            		modbus_reply_exception(ctx, query, MODBUS_EXCEPTION_ILLEGAL_FUNCTION);
		            		pthread_mutex_lock(&MODBus_mem_access);
								arg_pass.mb_RD->illegal_req_cnt++;
							pthread_mutex_unlock(&MODBus_mem_access);
							continue;
		            	}
						pthread_mutex_lock(&MODBus_mem_access);
							if(mb_write_funcs(mb_query->func_code))
								arg_pass.mb_RD->write_req_cnt++;
							else
								arg_pass.mb_RD->read_req_cnt++;
							if(modbus_reply(ctx, query, rc, MODBus_mem)>=0)
							{
								if(mb_write_funcs(mb_query->func_code))
								{
									if(!arg_pass.mb_RD->config_status.bits.config_not_saved)
										arg_pass.mb_RD->config_status.bits.config_not_saved = config_check(mb_query, config_start_addr, debug);
									if(arg_pass.mb_RDWR->control.bits.Save_config || arg_pass.mb_RDWR->control.bits.Restore_config)
									{
										if(arg_pass.mb_RDWR->control.bits.Save_config && !last_Save_config_bit &&
										   arg_pass.mb_RD->config_status.bits.config_not_saved)
											fls_app_config_file(&(arg_pass.mb_RDWR->config), "w");
										else if(arg_pass.mb_RDWR->control.bits.Restore_config && !last_Restore_config_bit)
										{
											if(fls_app_config_file(&(arg_pass.mb_RDWR->config), "r"))
												arg_pass.mb_RDWR->config = default_app_config;
										}
										//arg_pass.mb_RDWR->control.bits.Restore_config = false;
										//arg_pass.mb_RDWR->control.bits.Save_config = false;
										arg_pass.mb_RD->config_status.bits.config_not_saved = false;
									}
									last_Restore_config_bit = arg_pass.mb_RDWR->control.bits.Restore_config;
									last_Save_config_bit = arg_pass.mb_RDWR->control.bits.Save_config;
									if(arg_pass.mb_RDWR->control.bits.Reset_CNTs && !last_Reset_CNTs_bit)
									{
										arg_pass.mb_RD->poll_index = 0;
										arg_pass.mb_RD->read_req_cnt = 0;
										arg_pass.mb_RD->write_req_cnt = 0;
										arg_pass.mb_RD->illegal_req_cnt = 0;
										//arg_pass.mb_RDWR->control.bits.Reset_CNTs = false;
									}
									last_Reset_CNTs_bit = arg_pass.mb_RDWR->control.bits.Reset_CNTs;
								}
							}
						pthread_mutex_unlock(&MODBus_mem_access);
					}
					else
						modbus_reply_exception(ctx, query, MODBUS_EXCEPTION_NEGATIVE_ACKNOWLEDGE);
				}
                else if(rc==-1)// This example server in ended on connection closing or any errors.
				{
					if(verbose)
						printf("Connection closed on socket %d\n", master_socket);
					close(master_socket);
					FD_CLR(master_socket, &refset);//Remove from reference set
					if(master_socket == fdmax)
						fdmax--;
                }
            }
        }
    }
    if(verbose)
		printf("Program Exiting...\n");
	for(int i=0; i<n_threads; i++)
		pthread_join(thread_ids[i], NULL);
	if(server_socket != -1)
		close(server_socket);
    modbus_free(ctx);
    modbus_mapping_free(MODBus_mem);

	return EXIT_SUCCESS;
}

void print_usage(char *prog_name)
{
	const char preamp[] = {
	"\tProgram: fls_app  Copyright (C) 12021-12022  Sam Harry Tzavaras\n"
    "\tThis program comes with ABSOLUTELY NO WARRANTY; for details see LICENSE.\n"
    "\tThis is free software, and you are welcome to redistribute it\n"
    "\tunder certain conditions; for details see LICENSE.\n"
	};
	const char manual[] = {
		"Options:\n"
		"           -h : Print help.\n"
		"           -v : Verbose.\n"
		"           -d : Debug.\n"
		"           -p : TCP-Port.\n"
		"           -u : Unit-ID.\n"
		"           -V : Version.\n"
	};
	printf("%s\nUsage: %s [Options]\n\n%s",preamp, prog_name, manual);
}

unsigned int get_config_start_addr(mb_RDWR_regs *RDWR_regs)
{
	void *p_t1 = &(RDWR_regs->control);
	void *p_t2 = &(RDWR_regs->config);
	return (p_t2-p_t1)/sizeof(unsigned short);
}

bool config_check(MODBus_TCP_query *query, unsigned short config_start_addr, bool debug)
{
	unsigned short mb_rd_start_addr,
				   mb_rd_end_addr,
				   mb_wr_start_addr,
				   mb_wr_end_addr,
				   config_end_addr, i, buff, data_len;

	if(!query)
		return false;
	switch(query->func_code)
	{
		case MODBUS_FC_WRITE_SINGLE_REGISTER:
			mb_wr_start_addr = be16toh(query->data.mb_write_reg.reg_addr);
			mb_wr_end_addr = mb_wr_start_addr;
			if(debug)
			{
				printf("MB_Function: WRITE_SINGLE_REGISTER(%d)\n", query->func_code);
				printf("Register address: %d\n", 40001+mb_wr_start_addr);
				buff = be16toh(query->data.mb_write_reg.value);
				printf("Value: %d(0x%04X)\n", buff, buff);
			}
			break;
		case MODBUS_FC_MASK_WRITE_REGISTER:
			mb_wr_start_addr = be16toh(query->data.mb_mask_write_reg.reg_addr);
			mb_wr_end_addr = mb_wr_start_addr;
			if(debug)
			{
				printf("MB_Function: MASK_WRITE_REGISTER(%d)\n", query->func_code);
				printf("Register address: %d\n", 40001+mb_wr_start_addr);
				printf("And_mask: 0x%04X\n", be16toh(query->data.mb_mask_write_reg.And_mask));
				printf("Or_mask : 0x%04X\n", be16toh(query->data.mb_mask_write_reg.Or_mask));
			}
			break;
		case MODBUS_FC_WRITE_MULTIPLE_REGISTERS:
			mb_wr_start_addr = be16toh(query->data.mb_write_regs.start_write_reg_addr);
			mb_wr_end_addr = mb_wr_start_addr + be16toh(query->data.mb_write_regs.amount_of_regs_to_write)-1;
			if(debug)
			{
				printf("MB_Function: WRITE_MULTIPLE_REGISTERS(%d)\n", query->func_code);
				printf("Registers write range: %d-%d\n", 40001+mb_wr_start_addr, 40001+mb_wr_end_addr);
				printf("Amount of bytes: %d\n", query->data.mb_write_regs.write_byte_count);
				data_len = be16toh(query->data.mb_write_regs.amount_of_regs_to_write);
				for(i=0; i<data_len; i++)
				{
					buff = be16toh(query->data.mb_write_regs.data[i]);
					printf("Data_value[%d]: %d(0x%04X)\n", i, buff, buff);
				}
			}
			break;
		case MODBUS_FC_WRITE_AND_READ_REGISTERS:
			mb_rd_start_addr = be16toh(query->data.mb_read_write_regs.start_read_reg_addr);
			mb_rd_end_addr = mb_rd_start_addr + be16toh(query->data.mb_read_write_regs.amount_of_regs_to_read)-1;
			mb_wr_start_addr = be16toh(query->data.mb_read_write_regs.start_write_reg_addr);
			mb_wr_end_addr = mb_wr_start_addr + be16toh(query->data.mb_read_write_regs.amount_of_regs_to_write)-1;
			if(debug)
			{
				printf("MB_Function: WRITE_AND_READ_REGISTERS(%d)\n", query->func_code);
				printf("Registers read range:%d-%d\n", 40001+mb_rd_start_addr, 40001+mb_rd_end_addr);
				printf("Registers write range:%d-%d\n", 40001+mb_wr_start_addr, 40001+mb_wr_end_addr);
				printf("Amount of bytes: %d\n", query->data.mb_read_write_regs.write_byte_count);
				data_len = be16toh(query->data.mb_read_write_regs.amount_of_regs_to_write);
				for(i=0; i<data_len; i++)
				{
					buff = be16toh(query->data.mb_read_write_regs.data[i]);
					printf("Data_value[%d]: %d(0x%04X)\n", i, buff, buff);
				}
			}
			break;
		default: return false;
	}
	config_end_addr = config_start_addr + sizeof(fls_app_config)/sizeof(unsigned short)-1;
	return mb_wr_start_addr<=config_end_addr && mb_wr_end_addr >= config_start_addr;
}

unsigned char Checksum(const void *data, size_t data_size)
{
    unsigned char checksum = 0;
	unsigned char *data_dec = (unsigned char *)data;
    for (size_t i = 0; i < data_size; i++, data_dec++)
    {
        checksum = (checksum >> 1) + ((checksum & 1) << 7);
        checksum += *(data_dec);
    }
    return checksum;
}

int fls_app_config_file(fls_app_config *curr_config, const char *mode)
{
	struct {
		fls_app_config payload;
		unsigned char checksum;
	} __attribute__ ((packed)) config_file;
	unsigned char checksum;
	size_t read_bytes;
	FILE *fp;

	if(!curr_config || !mode)
		return EXIT_FAILURE;

	if(!strcmp(mode, "r"))
	{
		fp=fopen(config_file_path, mode);
		if(fp)
		{
			read_bytes = fread(&config_file, 1, sizeof(config_file), fp);
			fclose(fp);
			if(read_bytes == sizeof(config_file))
			{
				checksum = Checksum(&(config_file.payload), sizeof(fls_app_config));
				if(!(config_file.checksum ^ checksum))
					*curr_config = config_file.payload;
				else
					return EXIT_FAILURE;
			}
			else
				return EXIT_FAILURE;
		}
		else
			return EXIT_FAILURE;
	}
	else if(!strcmp(mode, "w"))
	{
		config_file.payload = *curr_config;
		config_file.checksum = Checksum(curr_config, sizeof(fls_app_config));
		fp=fopen(config_file_path, mode);
		if(fp)
		{
			fwrite(&config_file, 1, sizeof(config_file), fp);
			fclose(fp);
		}
		else
			return EXIT_FAILURE;
	}
	else
		return EXIT_FAILURE;
	return EXIT_SUCCESS;
}
