/*
File "Run_AVG_filter.c" part of flow sensors project,
contain implementations for function related to running average filter.
Copyright (C) 12021-12023  Sam harry Tzavaras.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <string.h>
#include "Run_AVG_filter.h"

unsigned short rAVG_filter(module_resources *res)
{
	unsigned short ret_val;

	if(!res || !(res->samples_buff) || !(res->current_sample) ||
	   !(res->module_resources_alloc) || !(res->module_resources_free) ||
	   !(res->module_samples_buff_summator) || !(res->module_samples_buff_averagor) ||
	   !(res->window_size))
		return 0;
	if(*(res->window_size)<=1)//Check for zero window size, and remove all samples on true.
	{
		if(!g_queue_is_empty(res->samples_buff))
			g_queue_clear_full(res->samples_buff, res->module_resources_free);
		return 1;
	}
	//Limit length of samples_buff to window size.
	while(g_queue_get_length(res->samples_buff) >= *(res->window_size))
		res->module_resources_free(g_queue_pop_tail(res->samples_buff));
	//Add current sample to samples_buff's head.
	g_queue_push_head(res->samples_buff, res->module_resources_alloc(res->current_sample));
	//Zero current_sample, Get sum of all samples, and calculate average.
	memset(res->current_sample, 0, res->current_sample_size);
	g_queue_foreach(res->samples_buff, res->module_samples_buff_summator, res->current_sample);
	res->module_samples_buff_averagor(res->current_sample, (ret_val = g_queue_get_length(res->samples_buff)));
	return ret_val;
}
