/*
File: DLHR.c Implementation of functions for DLHR DP sensors, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#define I2C_DEV_NAME "/dev/i2c-1"
#define DLHR_I2C_Addr 0x29

#define FS_24 16777216
#define mult_const 225.0
#define offset 40.0

#include <endian.h>

#include "meas_net_I2C.h"
#include "DLHR.h"

int meas_net_DLHR_get_meas(DLHR_mem *meas)
{
	if(!meas)
		return -1;
	if(I2C_read_bytes(I2C_DEV_NAME, DLHR_I2C_Addr, (unsigned char *)meas, sizeof(DLHR_mem)))
		return -1;
	#if defined(__BYTE_ORDER__)&&(__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
	meas->DP_code = be32toh(meas->DP_code)>>8;
	meas->Temp_code = be32toh(meas->Temp_code)>>8;
	#endif
	return 0;
}

int meas_net_DLHR_send_command(unsigned char com)
{
	switch(com)
	{
		case Start_Single :
		case Start_Avg_2  :
		case Start_Ang_4  :
		case Start_Avg_8  :
		case Start_Avg_16 :
			return I2C_write_byte(I2C_DEV_NAME, DLHR_I2C_Addr, com);
		default:
			return -1;
	}
}

float DLHR_DP_code_to_inWC(bool type, float FSS, int DP_code)
{
	int Offset;
	switch(type)
	{
		case DLHR_GAUGE_TYPE:
			Offset = FS_24/10;
			break;
		case DLHR_DIFF_TYPE:
			FSS = 2.0*FSS;
			Offset = FS_24/2;
			break;
	}
	return 1.25*((DP_code - Offset)/(float)FS_24)*FSS;
}

float DLHR_Temp_code_to_F(int Temp_code)
{
	return ((Temp_code * mult_const)/FS_24) - offset;
}
