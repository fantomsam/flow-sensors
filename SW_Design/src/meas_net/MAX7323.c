/*
File: MAX7323.h Implementation of functions for MAX7323, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#define I2C_DEV_NAME "/dev/i2c-1"

#include <endian.h>

#include "meas_net_I2C.h"
#include "MAX7323.h"

int MAX7323_set(MAX7323_io_reg out_val)
{
	return I2C_write_byte(I2C_DEV_NAME, FLS_APP_MAX7323_ADDR, *((unsigned char *)&out_val));
}

int MAX7323_get(MAX7323_io_reg *in_val)
{
	return I2C_read_byte(I2C_DEV_NAME, FLS_APP_MAX7323_ADDR, (unsigned char *)in_val);
}
