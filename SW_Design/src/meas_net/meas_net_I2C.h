/*
File: meas_net_I2C.c Declarations of I2C functions for mas_net, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
	/*-- All functions return: 0 on success or -1 on failure. --*/
//Function that read 1 byte from an I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name" and store it at "value_out".
int I2C_read_byte(char *i2c_dev_name, unsigned char dev_addr, unsigned char *value_out);
//Function that read len amount of byte from an I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name" and store it at "data".
int I2C_read_bytes(char *i2c_dev_name, unsigned char dev_addr, unsigned char *data, unsigned char len);
//Function that read a block "data" starting from address "reg" from an I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name".
int I2C_read_block(char *i2c_dev_name, unsigned char dev_addr, unsigned char reg, void *data, unsigned char len);

//Function that write 1 byte variable "value" to an I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name".
int I2C_write_byte(char *i2c_dev_name, unsigned char dev_addr, unsigned char value);
//Function that write len amount of byte to an I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name" and store it at "data".
int I2C_write_bytes(char *i2c_dev_name, unsigned char dev_addr, unsigned char *data, unsigned char len);
//Function that write block "data" to I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name".
int I2C_write_block(char *i2c_dev_name, unsigned char dev_addr, unsigned char reg, void *data, unsigned char len);


