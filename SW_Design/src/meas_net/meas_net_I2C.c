/*
File: meas_net_I2C.c Implementation of I2C functions for mas_net, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <asm/ioctl.h>

#include <arpa/inet.h>

#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <i2c/smbus.h>

//Function that read 1 byte from an I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name" and store it at "value_out".
//Return: 0 on success, -1 on failure.
int I2C_read_byte(char *i2c_dev_name, unsigned char dev_addr, unsigned char *value_out)
{
	int i2c_fd;//I2C file descriptor
	int write_bytes;
	//Open I2C-bus
	if((i2c_fd = open(i2c_dev_name, O_RDWR)) < 0)
	  return -1;
	if(ioctl(i2c_fd, I2C_SLAVE, dev_addr) < 0)
	{
	  close(i2c_fd);
	  return -1;
	}
	write_bytes = read(i2c_fd, value_out, 1);
	close(i2c_fd);
	return write_bytes == 1 ? 0 : -1;
}

//Function that read len amount of byte from an I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name" and store it at "value_out".
//Return: 0 on success, -1 on failure.
int I2C_read_bytes(char *i2c_dev_name, unsigned char dev_addr, unsigned char *value_out, unsigned char len)
{
	int i2c_fd;//I2C file descriptor
	int write_bytes;
	//Open I2C-bus
	if((i2c_fd = open(i2c_dev_name, O_RDWR)) < 0)
	  return -1;
	if(ioctl(i2c_fd, I2C_SLAVE, dev_addr) < 0)
	{
	  close(i2c_fd);
	  return -1;
	}
	write_bytes = read(i2c_fd, value_out, len);
	close(i2c_fd);
	return write_bytes == len ? 0 : -1;
}

//Function that read a block "data" from an I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name".
//Return: 0 on success, -1 on failure.
int I2C_read_block(char *i2c_dev_name, unsigned char dev_addr, unsigned char reg, void *data, unsigned char len)
{
	int ret_val, i2c_fd;//I2C file descriptor
	struct i2c_rdwr_ioctl_data msgset;
	//Open I2C-bus
	if((i2c_fd = open(i2c_dev_name, O_RDWR)) < 0)
	  return -1;
	//Allocate memory for the messages
	msgset.nmsgs = 2;
	if(!(msgset.msgs = calloc(msgset.nmsgs, sizeof(struct i2c_msg))))
	{
	  fprintf(stderr, "Memory error!!!\n");
	  exit(EXIT_FAILURE);
	}
	//Build message for Write reg
	msgset.msgs[0].addr = dev_addr;
	msgset.msgs[0].flags = 0; //Write
	msgset.msgs[0].len = 1;
	msgset.msgs[0].buf = &reg;
	//Build message for Read *data
	msgset.msgs[1].addr = dev_addr;
	msgset.msgs[1].flags = I2C_M_RD;//Read flag
	msgset.msgs[1].len = len;
	msgset.msgs[1].buf = data;
	//write reg and read the measurements
	ret_val = ioctl(i2c_fd, I2C_RDWR, &msgset);
	close(i2c_fd);
	free(msgset.msgs);
	return ret_val==msgset.nmsgs ? 0 : -1;
}

//Function that write 1 byte from variable "value" to an I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name".
//Return: 0 on success, -1 on failure.
int I2C_write_byte(char *i2c_dev_name, unsigned char dev_addr, unsigned char value)
{
	int i2c_fd;//I2C file descriptor
	int write_bytes;
	//Open I2C-bus
	if((i2c_fd = open(i2c_dev_name, O_RDWR)) < 0)
	  return -1;
	if(ioctl(i2c_fd, I2C_SLAVE, dev_addr) < 0)
	{
	  close(i2c_fd);
	  return -1;
	}
	write_bytes = write(i2c_fd, &value, 1);
	close(i2c_fd);
	return write_bytes == 1 ? 0 : -1;
}

//Function that write 1 byte variable "value" to an I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name".
//Return: 0 on success, -1 on failure.
int I2C_write_bytes(char *i2c_dev_name, unsigned char dev_addr, unsigned char *data, unsigned char len)
{
	int i2c_fd;//I2C file descriptor
	int writen_bytes;
	//Open I2C-bus
	if((i2c_fd = open(i2c_dev_name, O_RDWR)) < 0)
	  return -1;
	if(ioctl(i2c_fd, I2C_SLAVE, dev_addr) < 0)
	{
	  close(i2c_fd);
	  return -1;
	}
	writen_bytes = write(i2c_fd, data, len);
	close(i2c_fd);
	return writen_bytes == len ? 0 : -1;
}

//Function that write block "data" to I2C device with address "dev_addr" on I2C bus with name "i2c_dev_name".
//Return: 0 on success, -1 on failure.
int I2C_write_block(char *i2c_dev_name, unsigned char dev_addr, unsigned char reg, void *data, unsigned char len)
{
	int i2c_fd;//I2C file descriptor
	int writen_bytes;
	unsigned char *data_w_reg;
	//Open I2C-bus
	if((i2c_fd = open(i2c_dev_name, O_RDWR)) < 0)
	  return -1;
	if(ioctl(i2c_fd, I2C_SLAVE, dev_addr) < 0)
	{
	  close(i2c_fd);
	  return -1;
	}
	if(!(data_w_reg = calloc(len+1, sizeof(*data_w_reg))))
	{
	  fprintf(stderr, "Memory error!!!\n");
	  exit(EXIT_FAILURE);
	}
	data_w_reg[0] = reg;
	memcpy(data_w_reg+1, data, len);
	writen_bytes = write(i2c_fd, data_w_reg, len+1);
	free(data_w_reg);
	close(i2c_fd);
	return writen_bytes == len+1 ? 0 : -1;
}
