/*
File: BMP280.h Declarations of functions for BPM280 sensor, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef BMP280_H
#define BMP280_H

#define BMP280_CAL_DATA_ADDR 	0x88
#define BMP280_ID_ADDR 			0xD0
#define BMP280_RESET_ADDR 		0xE0
#define BMP280_MEM_ADDR 		0xF3
#define BMP280_CONFIG_REGS_ADDR 0xF4

#define BMP280_reset_value 0xB6;

#define BMP280_ODR_0_5_MS 	0x00
#define BMP280_ODR_62_5_MS 	0x01
#define BMP280_ODR_125_MS 	0x02
#define BMP280_ODR_250_MS   0x03
#define BMP280_ODR_500_MS   0x04
#define BMP280_ODR_1000_MS  0x05
#define BMP280_ODR_2000_MS  0x06
#define BMP280_ODR_4000_MS  0x07

#define BMP280_NONE	0x00
#define BMP280_1X   0x01
#define BMP280_2X   0x02
#define BMP280_4X   0x03
#define BMP280_8X   0x04
#define BMP280_16X  0x05

#define BMP280_FILTER_OFF 		0x00
#define BMP280_FILTER_COEFF_2 	0x01
#define BMP280_FILTER_COEFF_4 	0x02
#define BMP280_FILTER_COEFF_8   0x03
#define BMP280_FILTER_COEFF_16  0x04

#define BMP280_POWERMODE_SLEEP  0x00
#define BMP280_POWERMODE_FORCED 0x01
#define BMP280_POWERMODE_NORMAL 0x03

typedef unsigned char chip_id;

typedef struct BMP280_calib_data_struct{
	unsigned short dig_T1;
	short dig_T2, dig_T3;
	unsigned short dig_P1;
	short dig_P2, dig_P3, dig_P4, dig_P5, dig_P6, dig_P7, dig_P8, dig_P9;
} __attribute__ ((packed)) BMP280_calib_data;

typedef struct ctrl_and_config_struct{
	union ctrl_meas_union{
		struct {
			unsigned mode 			: 2;
			unsigned oversampling_p : 3;
			unsigned oversampling_t : 3;
		} __attribute__ ((packed)) bits;
		unsigned char as_byte;
	} __attribute__ ((packed)) ctrl_meas;
	union config_union{
		struct {
			unsigned spi_3w_en  : 1;
			unsigned res_0	    : 1;
			unsigned filter	    : 3;
			unsigned samplerate : 3;
		} __attribute__ ((packed)) bits;
		unsigned char as_byte;
	} __attribute__ ((packed)) config;
} __attribute__ ((packed)) BMP280_ctrl_and_config_regs;

typedef struct BMP280_mem_struct{
	union {
		struct {
			unsigned im_update : 1;
			unsigned res_0 	   : 2;
			unsigned measuring : 1;
			unsigned res_1	   : 3;
			unsigned Unconn    : 1;
		} __attribute__ ((packed)) bits;
		unsigned char as_byte;
	} __attribute__ ((packed)) status;
	BMP280_ctrl_and_config_regs ctrl_and_config;
	unsigned p_code : 24;
	unsigned t_code : 24;
} __attribute__ ((packed)) BMP280_mem;

	/*-- All functions return: 0 on success or -1 on failure. --*/
//Function that get calibration data from BMP280.
int meas_net_BMP280_get_cal_data(BMP280_calib_data *calib_data);
//Function that get chip_ID of BMP280.
int meas_net_BMP280_get_chip_id(chip_id *id);
//Function that get current measurements from BMP280.
int meas_net_BMP280_get_meas(BMP280_mem *meas);
//Function that reset the BMP280.
int meas_net_BMP280_reset();
//Function that set configuration registers of BMP280.
int meas_net_BMP280_set_config(const BMP280_ctrl_and_config_regs *config_regs);

double meas_net_BMP280_conv_comp_temp_F(const BMP280_mem *meas, BMP280_calib_data *cal_data, double *t_fine_out);
double meas_net_BMP280_conv_comp_press_inHg(const BMP280_mem *meas, BMP280_calib_data *cal_data, const double t_fine_out, const double temp_F);

#endif //BMP280_H
