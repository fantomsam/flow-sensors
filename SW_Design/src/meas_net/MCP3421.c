/*
File: MCP3421.c Implementation of functions for MCP3421, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#define I2C_DEV_NAME "/dev/i2c-1"

#include <endian.h>

#include "meas_net_I2C.h"
#include "MCP3421.h"

int MCP3421_get_meas(unsigned char MCP3421_addr, MCP3421_meas_frame *meas)
{
	if(!meas)
		return -1;
	if(!I2C_read_bytes(I2C_DEV_NAME, MCP3421_addr, (unsigned char *) meas, sizeof(MCP3421_meas_frame)))
	{
		#if defined(__BYTE_ORDER__)&&(__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
		meas->meas_code = be16toh(meas->meas_code);
		#endif
		return 0;
	}
	return -1;
}

int MCP3421_set_config(unsigned char MCP3421_addr, const MCP3421_config_reg *config)
{
	unsigned char config_byte;

	if(config)
	{
		config_byte = *((unsigned char *)config);
		return I2C_write_byte(I2C_DEV_NAME, MCP3421_addr, config_byte);
	}
	return -1;
}
