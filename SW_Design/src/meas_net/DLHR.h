/*
File: DLHR.h Declarations of functions for DLHR DP sensors, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef DLHR_H
#define DLHR_H

#define Okay_Status 0x40

#include <stdbool.h>

enum DLHR_types{ DLHR_GAUGE_TYPE = false, DLHR_DIFF_TYPE = true };

enum DLHR_Commands{
	Start_Single = 0xAA,
	Start_Avg_2  = 0xAC,
	Start_Ang_4  = 0xAD,
	Start_Avg_8  = 0xAE,
	Start_Avg_16 = 0xAF
};

typedef struct DLHR_mem_struct{
	union {
		struct {
			unsigned ALU_error : 1;
			unsigned Sens_conf : 1;
			unsigned Mem_Error : 1;
			unsigned Mode	   : 2;
			unsigned Busy	   : 1;
			unsigned Power 	   : 1;
			unsigned Unconn    : 1;
		} __attribute__ ((packed)) bits;
		unsigned char as_byte;
	} __attribute__ ((packed)) status;
	unsigned DP_code   : 24;
	unsigned Temp_code : 24;
} __attribute__ ((packed)) DLHR_mem;

	/*-- All functions return: 0 on success or -1 on failure. --*/
//Function that get measurement from DLHR sensor and store them to meas.
int meas_net_DLHR_get_meas(DLHR_mem *meas);
//Function that send a command to DLHR sensor.
int meas_net_DLHR_send_command(unsigned char com);

float DLHR_DP_code_to_inWC(bool type, float FSS, int DP_code);
float DLHR_Temp_code_to_F(int Temp_code);

#endif //DLHR_H
