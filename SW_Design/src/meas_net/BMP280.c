/*
File: BMP280.c Implementation of functions for BPM280 sensor, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#define I2C_DEV_NAME "/dev/i2c-1"
#define BMP280_I2C_Addr 0x76

#include <math.h>
#include <endian.h>

#include "meas_net_I2C.h"
#include "BMP280.h"

int meas_net_BMP280_get_cal_data(BMP280_calib_data *calib_data)
{
	if(!calib_data)
		return -1;
	if(I2C_read_block(I2C_DEV_NAME, BMP280_I2C_Addr, BMP280_CAL_DATA_ADDR, calib_data, sizeof(BMP280_calib_data)))
		return -1;
	#if defined(__BYTE_ORDER__)&&(__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)
	unsigned short *t = (unsigned short *)calib_data;
	for(int i=0; i<sizeof(BMP280_calib_data)/sizeof(short); i++)
		t[i] = le16toh(t[i]);
	#endif
	return 0;
}

int meas_net_BMP280_get_chip_id(chip_id *id)
{
	if(!id)
		return -1;
	return I2C_read_block(I2C_DEV_NAME, BMP280_I2C_Addr, BMP280_ID_ADDR, id, sizeof(chip_id));
}

int meas_net_BMP280_get_meas(BMP280_mem *meas)
{
	if(!meas)
		return -1;
	if(I2C_read_block(I2C_DEV_NAME, BMP280_I2C_Addr, BMP280_MEM_ADDR, meas, sizeof(BMP280_mem)))
		return -1;
	#if defined(__BYTE_ORDER__)&&(__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)
	meas->p_code = be32toh(meas->p_code);
	meas->t_code = be32toh(meas->t_code);
	#endif
	meas->p_code >>= 4;
	meas->t_code >>= 4;
	return 0;
}

int meas_net_BMP280_reset()
{
	unsigned char val = BMP280_reset_value;
	return I2C_write_block(I2C_DEV_NAME, BMP280_I2C_Addr, BMP280_RESET_ADDR, &val, sizeof(val));
}

int meas_net_BMP280_set_config(const BMP280_ctrl_and_config_regs *config_regs)
{
	unsigned char send_data[3];//Construct pairs of address and data. Auto-increment does not supported.
	send_data[0]=config_regs->ctrl_meas.as_byte;
	send_data[1]=BMP280_CONFIG_REGS_ADDR+1;
	send_data[2]=config_regs->config.as_byte;
	return I2C_write_block(I2C_DEV_NAME,
						   BMP280_I2C_Addr,
						   BMP280_CONFIG_REGS_ADDR,
						   send_data,
						   sizeof(send_data));
}

double meas_net_BMP280_conv_comp_temp_F(const BMP280_mem *meas, BMP280_calib_data *cal_data, double *t_fine_out)
{
	double var1 = (((double)meas->t_code)/16384.0 - ((double)cal_data->dig_T1)/1024.0) * ((double)cal_data->dig_T2);
	double var2 = ((((double)meas->t_code)/131072.0 - ((double)cal_data->dig_T1)/8192.0) *
				  (((double)meas->t_code)/131072.0 - ((double)cal_data->dig_T1)/8192.0)) *
				  ((double)cal_data->dig_T3);
	double t_fine = var1 + var2;
	if(t_fine_out)
		*t_fine_out = t_fine;
	return (t_fine*9.0/25600.0)+32.0;
}

double meas_net_BMP280_conv_comp_press_inHg(const BMP280_mem *meas, BMP280_calib_data *cal_data, const double t_fine_out, const double temp_F)
{
	double var1, var2, var_press, cal_press_psf, calc_rho_Hg;

	var1 = (t_fine_out / 2.0) - 64000.0;
	var2 = var1 * var1 * ((double) cal_data->dig_P6) / 32768.0;
	var2 = var2 + var1 * ((double) cal_data->dig_P5) * 2.0;
	var2 = (var2 / 4.0) + (((double) cal_data->dig_P4) * 65536.0);
	var1 = (((double)cal_data->dig_P3) * var1 * var1 / 524288.0 + ((double)cal_data->dig_P2) * var1) / 524288.0;
	var1 = (1.0 + var1 / 32768.0) * ((double)cal_data->dig_P1);

	var_press = 1048576.0 - (double)meas->p_code;
	var_press = (var_press - (var2 / 4096.0)) * 6250.0 / var1;
	var1 = ((double)cal_data->dig_P9) * var_press * var_press / 2147483648.0;
	var2 = var_press * ((double)cal_data->dig_P8) / 32768.0;

	if(isnan(temp_F))
		return (var_press + (var1 + var2 + ((double)cal_data->dig_P7)) / 16.0) * 0.0002953;

	cal_press_psf = (var_press + (var1 + var2 + ((double)cal_data->dig_P7)) / 16.0) * 0.020885434273039;
	calc_rho_Hg = (-0.0834020050125312)*temp_F + 850.876404010025;
	return (cal_press_psf/calc_rho_Hg)*12.0;

}
