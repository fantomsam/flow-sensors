/*
File: meas_net.c Implementation of Meas_net thread, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#define CPU_temp_sysfs_file "/sys/class/thermal/thermal_zone0/temp"
#define CNT_INIT 10

#define DP_MODULE_RANGE 5.0 //±5 inWC

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <pthread.h>

#include <gmodule.h>

#include <glib.h>
#include <glibtop.h>
#include <glibtop/uptime.h>
#include <glibtop/cpu.h>
#include <glibtop/mem.h>
#include <glibtop/fsusage.h>

#include "DLHR.h"
#include "BMP280.h"
#include "MCP3421.h"
#include "MAX7323.h"
#include "../fls_app_types.h"
#include "../Run_AVG_filter.h"

	/*-- External Global variables--*/
extern pthread_mutex_t MODBus_mem_access;
extern unsigned char running;

	/*-- Local Functions --*/
//Function that return Computer's stats: Uptime, CPU_usage, RAM_Util, Disk_Util, CPU_temp_F.
static int RPi_stats(mb_RD_regs *mb_RD);
//Function that interacting with PTH_Module.
static int PTH_Module_if(mb_RD_regs *mb_RD, mb_RDWR_regs *mb_RDWR);
//Functions that interacting with DP_Module.
static int DP_Module_if(mb_RD_regs *mb_RD, mb_RDWR_regs *mb_RDWR, bool Update_avg, GQueue *samples_buff);
void DP_mod_sample_buff_free(gpointer data);
//Function that interacting with HF_Module.
static int HF_Module_if(mb_RD_regs *mb_RD, mb_RDWR_regs *mb_RDWR, bool Update_avg, GQueue *samples_buff);
void HF_mod_sample_buff_free(gpointer data);

void *meas_net_thread(void *varg)
{
	struct arg_passer *args = (struct arg_passer*)varg;
	unsigned char cnt=0;
	bool PTH_not_pressent=true;
	clock_t t0, t1, t_wait;
	//GQueues sample_buffs for modules.
	GQueue *DP_mod_sample_buff = g_queue_new(),
		   *HF_mod_sample_buff = g_queue_new();

	while(running)
	{
		t0 = clock();
		if(!cnt)
		{
			RPi_stats(args->mb_RD);
			cnt = CNT_INIT-1;
		}
		else
			cnt--;
		if(!cnt || PTH_not_pressent)
			PTH_not_pressent = PTH_Module_if(args->mb_RD, args->mb_RDWR);
		DP_Module_if(args->mb_RD, args->mb_RDWR, !cnt, DP_mod_sample_buff);
		HF_Module_if(args->mb_RD, args->mb_RDWR, !cnt, HF_mod_sample_buff);
		t1 = clock();
		if((t_wait = 100000-(t1-t0))>0)
			usleep(t_wait);//Sleep for t_wait (100ms - time_used_by_modules).
	}
	g_queue_free_full(DP_mod_sample_buff, DP_mod_sample_buff_free);
	g_queue_free_full(HF_mod_sample_buff, HF_mod_sample_buff_free);
	return NULL;
}

static int PTH_Module_if(mb_RD_regs *mb_RD, mb_RDWR_regs *mb_RDWR)
{
	//PTH_module related variables
	BMP280_mem PTH_mod_meas = {0};
	static bool PTH_init = false;
	static const BMP280_ctrl_and_config_regs BMP280_config = {
		//"ctrl_meas" register.
		.ctrl_meas.bits.mode = BMP280_POWERMODE_NORMAL,
		.ctrl_meas.bits.oversampling_p = BMP280_16X,
		.ctrl_meas.bits.oversampling_t = BMP280_2X,
		//"config" register.
		.config.bits.filter = BMP280_FILTER_COEFF_4,
		.config.bits.samplerate = BMP280_ODR_500_MS
	};
	static BMP280_calib_data cal_data={0};
	double amb_temp_F, amb_press_inHg, abs_amb_temp_R, pAir_lbcft, cAir_fps, t_fine;

	if(!meas_net_BMP280_get_meas(&PTH_mod_meas))
	{
		if(!PTH_init ||
		   !( PTH_mod_meas.ctrl_and_config.ctrl_meas.as_byte ||
		   PTH_mod_meas.ctrl_and_config.config.as_byte))
		{
			meas_net_BMP280_get_cal_data(&cal_data);
			meas_net_BMP280_set_config(&BMP280_config);
			PTH_init = true;
		}
		else
		{
			pthread_mutex_lock(&MODBus_mem_access);
				bool disable_PTH_corr_bit = mb_RDWR->control.bits.Dis_press_corr;
			pthread_mutex_unlock(&MODBus_mem_access);
			amb_temp_F = meas_net_BMP280_conv_comp_temp_F(&PTH_mod_meas, &cal_data, &t_fine);
			amb_press_inHg = meas_net_BMP280_conv_comp_press_inHg(&PTH_mod_meas, &cal_data, t_fine, disable_PTH_corr_bit?amb_temp_F:NAN);
			abs_amb_temp_R = amb_temp_F + 460.0;
			pAir_lbcft = 1.325 * amb_press_inHg/abs_amb_temp_R;
			cAir_fps = 49.03 * sqrt(abs_amb_temp_R);
			pthread_mutex_lock(&MODBus_mem_access);
				mb_RD->status_PTH_module = PTH_mod_meas.status.as_byte;
				mb_RD->amb_press_inHg = amb_press_inHg;
				mb_RD->amb_temp_F = amb_temp_F;
				mb_RD->pAir_lbcft = pAir_lbcft;
				mb_RD->cAir_fps = cAir_fps;
			pthread_mutex_unlock(&MODBus_mem_access);
			return 0;
		}
	}
	else
	{
		PTH_mod_meas.status.as_byte = 0;
		PTH_mod_meas.status.bits.Unconn = true;
	}
	pthread_mutex_lock(&MODBus_mem_access);
		mb_RD->status_PTH_module = PTH_mod_meas.status.as_byte;
	pthread_mutex_unlock(&MODBus_mem_access);
	return -1;
}

gpointer DP_module_resources_alloc(void *current_sample)
{
	gpointer data = g_slice_new(float);

	memcpy(data, current_sample, sizeof(float));
	return data;
}

void DP_module_samples_buff_summator(gpointer data, gpointer user_data)
{
	float *data_float = (float *)data, *acc = (float *)user_data;

	*acc += *data_float;
}

void DP_module_samples_buff_averagor(void *data, unsigned short window_size)
{
	float *data_float = (float *)data;

	if(window_size)
		*data_float /= window_size;
}

void DP_mod_sample_buff_free(gpointer data)
{
	if(data)
		g_slice_free(float, data);
}

static int DP_Module_if(mb_RD_regs *mb_RD, mb_RDWR_regs *mb_RDWR, bool Update_avg, GQueue *samples_buff)
{
	int ret;
	control_reg control;
	static bool last_DP_module_zero_bit = false;
	fls_app_config app_config;
	static unsigned char DP_inWC_acc_cnt = 0;
	static double DP_inWC_acc=0;
	unsigned short DP_mod_run_AVG_cnt;
	float DP_inWC_avg, DP_inWC, VP_inWC, DP_mod_fpm, DP_mod_cfm, DP_mod_lbpm;
	DLHR_mem DP_mod_meas = {0};
	//Variables for ambient measurements. (from PTH_Module)
	float pAir_lbcft;
	//Module_resources struct for rAVG_filter.
	module_resources res = {
		.samples_buff = samples_buff,
		.current_sample = &DP_inWC,
		.current_sample_size = sizeof(DP_inWC),
		.module_resources_alloc = DP_module_resources_alloc,
		.module_resources_free = DP_mod_sample_buff_free,
		.module_samples_buff_summator = DP_module_samples_buff_summator,
		.module_samples_buff_averagor = DP_module_samples_buff_averagor,
		.window_size = &(app_config.DP_mod_run_AVG_win_size)
	};

	pthread_mutex_lock(&MODBus_mem_access);
		control = mb_RDWR->control;
		app_config = mb_RDWR->config;
		pAir_lbcft = mb_RD->pAir_lbcft;
	pthread_mutex_unlock(&MODBus_mem_access);
	//Get meas from DP_Module.
	if(!meas_net_DLHR_get_meas(&DP_mod_meas))
	{
		if(control.bits.Global_on_off && control.bits.DP_module_on_off)
		{
			meas_net_DLHR_send_command(Start_Avg_16);
			DP_inWC = DLHR_DP_code_to_inWC(DLHR_DIFF_TYPE, DP_MODULE_RANGE, DP_mod_meas.DP_code);
			DP_inWC = roundf(DP_inWC * 1e4)/1e4;
			DP_inWC_acc += DP_inWC;
			DP_inWC_acc_cnt++;
			DP_mod_run_AVG_cnt = rAVG_filter(&res);
			DP_inWC -= app_config.DP_Module_meas_offset;
			VP_inWC = DP_inWC/app_config.DP_module_Mag_factor;
			DP_mod_fpm = copysignf(1096.7 * sqrtf(fabsf(VP_inWC)/pAir_lbcft), VP_inWC);
			DP_mod_cfm = app_config.Duct_area_sqft * DP_mod_fpm;
			DP_mod_lbpm = pAir_lbcft * DP_mod_cfm;
			if(Update_avg && DP_inWC_acc_cnt)//Calculate DP_inWC_avg
			{
				DP_inWC_avg = roundf((DP_inWC_acc/DP_inWC_acc_cnt) * 1e4)/1e4;
				if(control.bits.DP_module_zero && !last_DP_module_zero_bit)
				{
					pthread_mutex_lock(&MODBus_mem_access);
						mb_RDWR->config.DP_Module_meas_offset = DP_inWC_avg;
						app_config = mb_RDWR->config;
						mb_RD->config_status.bits.config_not_saved = true;
					pthread_mutex_unlock(&MODBus_mem_access);
				}
				last_DP_module_zero_bit = control.bits.DP_module_zero;
				DP_inWC_avg -= app_config.DP_Module_meas_offset;
				pthread_mutex_lock(&MODBus_mem_access);
					mb_RD->DP_inWC_avg = DP_inWC_avg;
				pthread_mutex_unlock(&MODBus_mem_access);
				DP_inWC_acc = 0;
				DP_inWC_acc_cnt = 0;
			}
			//Update DP_module related variables to MODBus space
			pthread_mutex_lock(&MODBus_mem_access);
				mb_RD->DP_mod_run_AVG_cnt = DP_mod_run_AVG_cnt;
				mb_RD->status_DP_module = DP_mod_meas.status.as_byte;
				mb_RD->DP_inWC = DP_inWC;
				mb_RD->VP_inWC = VP_inWC;
				mb_RD->DP_module_fpm = DP_mod_fpm;
				mb_RD->DP_Module_cfm = DP_mod_cfm;
				mb_RD->DP_Module_lbm = DP_mod_lbpm;
			pthread_mutex_unlock(&MODBus_mem_access);
			return 0;
		}
		ret = 1;
	}
	else
	{
		DP_mod_meas.status.as_byte = 0;
		DP_mod_meas.status.bits.Unconn = true;
		ret = -1;
	}
	DP_inWC_acc = 0;
	DP_inWC_acc_cnt = 0;
	if(!g_queue_is_empty(samples_buff))
		g_queue_clear_full(samples_buff, DP_mod_sample_buff_free);
	pthread_mutex_lock(&MODBus_mem_access);
		mb_RD->DP_mod_run_AVG_cnt = 0;
		mb_RD->status_DP_module = DP_mod_meas.status.as_byte;
		mb_RD->DP_inWC = NAN;
		mb_RD->DP_inWC_avg = NAN;
		mb_RD->VP_inWC = NAN;
		mb_RD->DP_module_fpm = NAN;
		mb_RD->DP_Module_cfm = NAN;
		mb_RD->DP_Module_lbm = NAN;
	pthread_mutex_unlock(&MODBus_mem_access);
	return ret;
}

#define HF_MOD_ADC0_ADDR 0x6a
#define HF_MOD_ADC1_ADDR 0x6b

typedef struct HF_sample_data_str{
	float ADCs_meas_mV[2];
} HF_sample_data;

gpointer HF_module_resources_alloc(void *current_sample)
{
	gpointer data = g_slice_new(HF_sample_data);

	memcpy(data, current_sample, sizeof(HF_sample_data));
	return data;
}

void HF_module_samples_buff_summator(gpointer data, gpointer user_data)
{
	HF_sample_data *data_HF_sample_data = (HF_sample_data *)data;
	float *HF_samples_acc = (float *)user_data;

	for(int i=0; i<2; i++)
		HF_samples_acc[i] += data_HF_sample_data->ADCs_meas_mV[i];
}

void HF_module_samples_buff_averagor(void * data, unsigned short window_size)
{
	float *data_float = (float *)data;

	if(window_size)
		for(int i=0; i<2; i++)
			data_float[i] /= window_size;
}

void HF_mod_sample_buff_free(gpointer data)
{
	if(data)
		g_slice_free(HF_sample_data, data);
}

static int HF_Module_if(mb_RD_regs *mb_RD, mb_RDWR_regs *mb_RDWR, bool Update_avg, GQueue *samples_buff)
{
	int ret=0;
	control_reg control;
	fls_app_config app_config;
	float pAir_lbcft; //Variable for ambient measurements. (from PTH_Module)
	//-- HF_Module related variables --//
	static const MCP3421_config_reg init_config = {
		.PGA_gain = PGA_x8,
		.Sample_rate = MCP3421_15SPS,
		.oneshot_cont = MCP3421_Cont_Conv,
	};
	static bool last_HF_module_zero_bit = false;
	union HF_Module_status_union{
		struct HF_Module_status_struct{
			unsigned H0_error:	  1;
			unsigned H1_error:	  1;
			unsigned Direction:	  1;
			unsigned RES_0: 	  4;
			unsigned Unconn:	  1;
		} __attribute__ ((packed)) bits;
		unsigned short as_short;
	} HF_module_status;
	const unsigned char *init_config_byte = (unsigned char *)&init_config;
	MAX7323_io_reg HF_Module_ctrl = {0};
	MCP3421_meas_frame meas = {0};
	unsigned char *curr_config_byte, HF_Module_ADC_addr, i;
	float ADCs_meas_mV[2], ADCs_meas_mV_avg[2]={0}, HF_Module_fpm, HF_Module_cfm, HF_Module_lbm;
	static double ADCs_meas_mV_acc[2]={0};
	static unsigned char ADCs_meas_mV_acc_cnt = 0;
	unsigned short HF_mod_run_AVG_cnt;
	//Module_resources struct for rAVG_filter.
	module_resources res = {
		.samples_buff = samples_buff,
		.current_sample = ADCs_meas_mV,
		.current_sample_size = sizeof(HF_sample_data),
		.module_resources_alloc = HF_module_resources_alloc,
		.module_resources_free = HF_mod_sample_buff_free,
		.module_samples_buff_summator = HF_module_samples_buff_summator,
		.module_samples_buff_averagor = HF_module_samples_buff_averagor,
		.window_size = &(app_config.HF_mod_run_AVG_win_size)
	};

	pthread_mutex_lock(&MODBus_mem_access);
		control = mb_RDWR->control;
		app_config = mb_RDWR->config;
		pAir_lbcft = mb_RD->pAir_lbcft;
	pthread_mutex_unlock(&MODBus_mem_access);

	if(!(ret = MAX7323_get(&HF_Module_ctrl)) && (control.bits.Global_on_off && control.bits.HF_module_on_off))
	{
		HF_module_status.as_short = 0;
		HF_Module_ctrl.H0_on_off = !control.bits.HF_module_on_off;
		HF_Module_ctrl.H1_on_off = !control.bits.HF_module_on_off;
		HF_Module_ctrl.Act_LED = !HF_Module_ctrl.Act_LED;
		MAX7323_set(HF_Module_ctrl);
		//Get voltage measurement from ADCs (MCP3421).
		for(i=0; i<2; i++)
		{
			switch(i)
			{
				case 0:	HF_Module_ADC_addr = HF_MOD_ADC0_ADDR; break;
				case 1:	HF_Module_ADC_addr = HF_MOD_ADC1_ADDR; break;
				default: break;
			}
			if(!MCP3421_get_meas(HF_Module_ADC_addr, &meas))
			{
				curr_config_byte = (unsigned char *)&meas.config;
				if(*curr_config_byte == *init_config_byte)
				{
					if(meas.meas_code == 0x7FFF || meas.meas_code == 0x8000)//Check for over-range
						HF_module_status.as_short |= 1<<i; //Set error bit on over-range
					ADCs_meas_mV[i] = (meas.meas_code/32767.0 * MCP3421_Vref);
					ADCs_meas_mV_acc[i] += ADCs_meas_mV[i];
				}
				else if(MCP3421_set_config(HF_Module_ADC_addr, &init_config))
					HF_module_status.as_short |= 1<<i;
			}
			else
				HF_module_status.as_short |= 1<<i;
		}
		ADCs_meas_mV_acc_cnt++;
				//-- Calculations --//
		if(Update_avg && ADCs_meas_mV_acc_cnt) //Calculate ADCs_meas_mV_avg
		{
			for(i=0; i<2; i++)
			{
				ADCs_meas_mV_avg[i] = ADCs_meas_mV_acc[i]/ADCs_meas_mV_acc_cnt;
				ADCs_meas_mV_acc[i] = 0;
				if(control.bits.HF_module_zero && !last_HF_module_zero_bit)
				{
					pthread_mutex_lock(&MODBus_mem_access);
						mb_RDWR->config.HF_module_meas_offset[i] = ADCs_meas_mV_avg[i];
						mb_RD->config_status.bits.config_not_saved = true;
						app_config = mb_RDWR->config;
					pthread_mutex_unlock(&MODBus_mem_access);
				}
				ADCs_meas_mV_avg[i] -= app_config.HF_module_meas_offset[i];
			}
			last_HF_module_zero_bit = control.bits.HF_module_zero;
			ADCs_meas_mV_acc_cnt = 0;
		}
		//Calculate Airflow.
		HF_mod_run_AVG_cnt = rAVG_filter(&res);
		for(i=0; i<2; i++)
			ADCs_meas_mV[i] -= app_config.HF_module_meas_offset[i];
		HF_module_status.bits.Direction = ADCs_meas_mV[0]<ADCs_meas_mV[1];
		HF_Module_fpm = fabsf(!HF_module_status.bits.Direction ? ADCs_meas_mV[0]:ADCs_meas_mV[1]);
		HF_Module_fpm = (powf(HF_Module_fpm, app_config.HF_Module_n_pow)*app_config.HF_Module_gain)+app_config.HF_Module_offset;
		HF_Module_fpm = HF_module_status.bits.Direction?-HF_Module_fpm:HF_Module_fpm;
		HF_Module_cfm = app_config.Duct_area_sqft * HF_Module_fpm;
		HF_Module_lbm = HF_Module_cfm * pAir_lbcft;
			//-- Load Measurements and calculation to MODBus --//
		pthread_mutex_lock(&MODBus_mem_access);
			mb_RD->HF_mod_run_AVG_cnt = HF_mod_run_AVG_cnt;
			mb_RD->status_HF_module = HF_module_status.as_short;
			for(i=0; i<2; i++)
			{
				mb_RD->HF_Module_ADCs_meas_mV[i] = ADCs_meas_mV[i];
				if(Update_avg)
					mb_RD->HF_Module_ADCs_meas_mV_avg[i] = ADCs_meas_mV_avg[i];
			}
			mb_RD->HF_Module_fpm = HF_Module_fpm;
			mb_RD->HF_Module_cfm = HF_Module_cfm;
			mb_RD->HF_Module_lbm = HF_Module_lbm;
		pthread_mutex_unlock(&MODBus_mem_access);
	}
	else
	{
		HF_module_status.as_short = 0;
		if(ret)
			HF_module_status.bits.Unconn = 1;
		else
		{
			HF_module_status.bits.Unconn = 0;
			curr_config_byte = (unsigned char *) &HF_Module_ctrl;
			*curr_config_byte = 0xff;
			MAX7323_set(HF_Module_ctrl);
		}
		for(i=0; i<2; i++)
			ADCs_meas_mV_acc[i] = 0;
		ADCs_meas_mV_acc_cnt = 0;
		if(!g_queue_is_empty(samples_buff))
			g_queue_clear_full(samples_buff, HF_mod_sample_buff_free);
		pthread_mutex_lock(&MODBus_mem_access);
			if(ret || !(control.bits.HF_module_on_off && control.bits.Global_on_off))
			{
				mb_RD->HF_mod_run_AVG_cnt = 0;
				for(i=0; i<2; i++)
				{
					mb_RD->HF_Module_ADCs_meas_mV[i] = NAN;
					mb_RD->HF_Module_ADCs_meas_mV_avg[i] = NAN;
				}
				mb_RD->HF_Module_fpm = NAN;
				mb_RD->HF_Module_cfm = NAN;
				mb_RD->HF_Module_lbm = NAN;
			}
			mb_RD->status_HF_module = HF_module_status.as_short;
		pthread_mutex_unlock(&MODBus_mem_access);
	}
	return ret;
}

static int RPi_stats(mb_RD_regs *mb_RD)
{
	unsigned int Up_time;
	FILE *CPU_temp_fp;
	char cpu_temp_str[20];
	struct system_stats sys_stats = {0};
	static glibtop_cpu buff_cpuload_before={0};
	glibtop_cpu buff_cpuload_after={0};
	glibtop_uptime buff_uptime;
	glibtop_mem buff_ram;
	glibtop_fsusage buff_disk;

	//Read values
	glibtop_get_uptime (&buff_uptime);//get computer's Up_time
	glibtop_get_mem (&buff_ram);//get ram util
	glibtop_get_cpu (&buff_cpuload_after);//get cpu util
	glibtop_get_fsusage (&buff_disk, "/");
	//Calc Uptime
	Up_time=buff_uptime.uptime;
	sys_stats.Up_time_days = Up_time/86400; Up_time%=86400;//get days from Up_time
	sys_stats.Up_time_hours = Up_time/3600; Up_time%=3600;//get hours from Up_time
	sys_stats.Up_time_min = Up_time/60; Up_time%=60;//get minutes from Up_time
	sys_stats.Up_time_sec = Up_time;//get seconds from Up_time
	//Calc CPU Utilization. Using current and old sample
	sys_stats.CPU_Util=100.0*(buff_cpuload_after.user-buff_cpuload_before.user);
	sys_stats.CPU_Util/=(buff_cpuload_after.total-buff_cpuload_before.total);
	//store current CPU stat sample to old
	memcpy(&buff_cpuload_before, &buff_cpuload_after, sizeof(glibtop_cpu));
	//Calc ram utilization
	sys_stats.RAM_Util=(buff_ram.used - buff_ram.buffer - buff_ram.cached) * 100.0 / buff_ram.total;
	//Calc Disk Utilization
	sys_stats.Disk_Util=(buff_disk.blocks - buff_disk.bavail) * 100.0 / buff_disk.blocks;
	//Read CPU Temp from sysfs
	if((CPU_temp_fp = fopen(CPU_temp_sysfs_file, "r")))
	{
		fscanf(CPU_temp_fp, "%s", cpu_temp_str);
		fclose(CPU_temp_fp);
		sys_stats.CPU_temp = (atof(cpu_temp_str) / 1E3) * 1.8 + 32.0; //Convert to Fahrenheit.
	}
	else
		sys_stats.CPU_temp = NAN;
	//Update RPi_stats variables to MODBus space.
	pthread_mutex_lock(&MODBus_mem_access);
		mb_RD->Up_time_days = sys_stats.Up_time_days;
		mb_RD->Up_time_hours = sys_stats.Up_time_hours;
		mb_RD->Up_time_min = sys_stats.Up_time_min;
		mb_RD->Up_time_sec = sys_stats.Up_time_sec;
		mb_RD->CPU_Util = sys_stats.CPU_Util;
		mb_RD->RAM_Util = sys_stats.RAM_Util;
		mb_RD->Disk_Util = sys_stats.Disk_Util;
		mb_RD->CPU_temp = sys_stats.CPU_temp;
	pthread_mutex_unlock(&MODBus_mem_access);
	return 0;
}
