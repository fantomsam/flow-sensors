/*
File: MCP3421.h Declarations of functions for MCP3421, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MCP3421_H
#define MCP3421_H

#define MCP3421_Vref 2048

enum MCP3421_PGA{
	PGA_x1 = 0,
	PGA_x2,
	PGA_x4,
	PGA_x8
};

enum MCP3421_Sample_rate{
	MCP3421_240SPS = 0,
	MCP3421_60SPS,
	MCP3421_15SPS,
	MCP3421_3_75SPS
};

enum MCP3421_Conv_mode{
	MCP3421_One_Shot = 0,
	MCP3421_Cont_Conv
};

typedef struct MCP3421_config_reg_struct{
	unsigned PGA_gain: 	   2;
	unsigned Sample_rate:  2;
	unsigned oneshot_cont: 1;
	unsigned RES:		   2;
	unsigned READY:		   1;
} __attribute__ ((packed)) MCP3421_config_reg;

typedef struct MCP3421_meas_frame_struct{
	short meas_code;
	MCP3421_config_reg config;
} __attribute__ ((packed)) MCP3421_meas_frame;

int MCP3421_get_meas(unsigned char MCP3421_addr, MCP3421_meas_frame *meas);
int MCP3421_set_config(unsigned char MCP3421_addr, const MCP3421_config_reg *config);
#endif //MCP3421_H
