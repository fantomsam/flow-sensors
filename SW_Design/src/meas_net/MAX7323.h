/*
File: MAX7323.h Declarations of functions for MAX7323, Part of flow sensors project.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef MAX7323_H
#define MAX7323_H

#define FLS_APP_MAX7323_ADDR 0x6d

typedef struct MAX7323_out_reg_struct{
	unsigned H0_on_off: 1;
	unsigned H1_on_off: 1;
	unsigned RES:		5;
	unsigned Act_LED:	1;
} __attribute__ ((packed)) MAX7323_io_reg;

int MAX7323_set(MAX7323_io_reg out_val);
int MAX7323_get(MAX7323_io_reg *in_val);

#endif //MAX7323_H
