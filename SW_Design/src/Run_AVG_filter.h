/*
File "Run_AVG_filter.h" part of flow sensors project, contain the definitions
of function and datatype for project's running average filter.
Copyright (C) 12021-12023  Sam harry Tzavaras.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef RUN_AVG_FILTER_H
#define RUN_AVG_FILTER_H

#include <gmodule.h>

typedef struct module_resources_str{
	GQueue *samples_buff;
	void *current_sample;
	size_t current_sample_size;
	gpointer (*module_resources_alloc)(void *current_sample);
	void (*module_resources_free)(gpointer data);
	void (*module_samples_buff_summator)(gpointer data, gpointer user_data);
	void (*module_samples_buff_averagor)(void * data, unsigned short window_size);
	unsigned short *window_size;
} module_resources;

/*
	Function rAVG_filter(): use the GQueue *samples_buff (part of module_resources)
	and create a history samples buffer that used via the module_samples_buff_summator()
	and module_samples_buff_averagor() to produce the running average value.

	The output running average value stored at the "current_sample"; the size of samples buffer
	limited to window_size value.

	Return the current of the samples_buff, or 0 at failure.
	** if (window_size <= 1) the filter does not run, the samples_buff is cleaned and return 1.
*/
unsigned short rAVG_filter(module_resources *res);

#endif //RUN_AVG_FILTER_H
