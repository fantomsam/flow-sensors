/*
File "fls_app_types.h" part of flow sensors project, contain the shared Datatypes.
Copyright (C) 12021-12022  Sam harry Tzavaras

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef FLS_APP_TYPES_H
#define FLS_APP_TYPES_H

#define true 1
#define false 0

#include <modbus.h>

//Application's macros
#define mb_accepted_funcs(fn) ( fn == MODBUS_FC_READ_HOLDING_REGISTERS \
							  ||fn == MODBUS_FC_READ_INPUT_REGISTERS \
							  ||fn == MODBUS_FC_WRITE_SINGLE_REGISTER \
							  ||fn == MODBUS_FC_WRITE_MULTIPLE_REGISTERS \
							  ||fn == MODBUS_FC_MASK_WRITE_REGISTER \
							  ||fn == MODBUS_FC_WRITE_AND_READ_REGISTERS)

#define mb_write_funcs(fn) ( fn == MODBUS_FC_WRITE_SINGLE_REGISTER \
						   ||fn == MODBUS_FC_WRITE_MULTIPLE_REGISTERS \
						   ||fn == MODBUS_FC_MASK_WRITE_REGISTER \
						   ||fn == MODBUS_FC_WRITE_AND_READ_REGISTERS)

#define mb_mem_size(elem) sizeof(elem)/sizeof(unsigned short)

				/*-- Application's Data structs --*/
#pragma pack(push, 2)//use pragma pack() to pack the following structs to 2 byte (16bit).

//Struct for decode/encode MODBus input registers.
typedef union config_status_reg_union{
	struct config_status_reg_struct{
		unsigned config_not_saved: 		    1;
		unsigned Ultrasonic_calc_Error: 	1;
	} bits;
	unsigned short as_short;
} config_status_reg;

typedef struct mb_RD_regs_map_struct {
	config_status_reg config_status;
	unsigned short poll_index;
	unsigned short read_req_cnt;
	unsigned short write_req_cnt;
	unsigned short illegal_req_cnt;
	unsigned short DP_mod_run_AVG_cnt;
	unsigned short HF_mod_run_AVG_cnt;
	unsigned short Ultrasonic_mod_run_AVG_cnt;
	unsigned short RES_0[1];
	//RPi Stats (Start_Addr:30010)
	unsigned short Up_time_days, Up_time_hours, Up_time_min, Up_time_sec;
	float CPU_Util, RAM_Util, Disk_Util, CPU_temp;
	unsigned short RES_1[8];
	//PTH_module (Start_Addr:30030)
	unsigned short status_PTH_module;
	float amb_temp_F, amb_press_inHg, pAir_lbcft, cAir_fps;
	unsigned short RES_2[1];
	//DP_module (Start_Addr:30040)
	unsigned short status_DP_module;
	float DP_inWC, DP_inWC_avg, VP_inWC, DP_module_fpm, DP_Module_cfm, DP_Module_lbm;
	unsigned short RES_3[7];
	//HF_module (Start_Addr:30060)
	unsigned short status_HF_module;
	float HF_Module_ADCs_meas_mV[2], HF_Module_ADCs_meas_mV_avg[2], HF_Module_fpm, HF_Module_cfm, HF_Module_lbm;
	unsigned short RES_4[5];
	//Ultrasonic flow meter (Start_Addr:30080)
	unsigned short status_MAX35104;
	float TOF_Up_ms, TOF_Down_ms, TOF_Diff_us, Air_speed_fpm, Vol_flow_cfm, Mass_flow_lbm,
		  t1_per_t2_Up, t2_per_t_ideal_Up, t1_per_t2_Down, t2_per_t_ideal_Down,
		  calc_velocity_press_inWC, virt_Air_temp_F, AVG_Air_speed_fpm, AVG_Vol_flow_cfm, AVG_Mass_flow_lbm;
} mb_RD_regs;

//Struct for decode/encode MODBus read/write registers.
typedef struct fls_app_config_struct {
	float Duct_area_sqft;
	float DP_Module_meas_offset;
	float DP_module_Mag_factor;
	float Ultrasonic_sensors_len_inch;
	float Ultrasonic_sensors_cos_a;
	float HF_module_meas_offset[2];
	float HF_Module_offset;
	float HF_Module_gain;
	float HF_Module_n_pow;
	unsigned short DP_mod_run_AVG_win_size;
	unsigned short HF_mod_run_AVG_win_size;
	unsigned short Ultrasonic_mod_run_AVG_win_size;
}fls_app_config;

static const fls_app_config default_app_config = {
	.Duct_area_sqft=1.0,
	.DP_Module_meas_offset=0,
	.DP_module_Mag_factor=1.0,
	.Ultrasonic_sensors_len_inch=12.0,
	.Ultrasonic_sensors_cos_a=1.0,
	.HF_module_meas_offset[0]=0,
	.HF_module_meas_offset[1]=0,
	.HF_Module_offset=0,
	.HF_Module_gain=1.0,
	.HF_Module_n_pow=1.0,
	.DP_mod_run_AVG_win_size=0,
	.HF_mod_run_AVG_win_size=0,
	.Ultrasonic_mod_run_AVG_win_size=0,
};

typedef union app_control_bits_union {
	struct app_control_bits_struct {
		//ON-OFF control bits.
		unsigned Global_on_off: 	1;
		unsigned Ultrasonic_on_off: 1;
		unsigned DP_module_on_off: 	1;
		unsigned HF_module_on_off:	1;
		unsigned RES_0:				4;
		//Zeroing bits;
		unsigned DP_module_zero: 	1;
		unsigned HF_module_zero: 	1;
		unsigned Dis_press_corr:	1;
		unsigned Ultrasonic_calc: 	1;
		unsigned RES_2:				1;
		unsigned Reset_CNTs:		1;
		unsigned Restore_config:	1;
		unsigned Save_config:		1;
	} __attribute__ ((packed)) bits;
	unsigned short as_short;
} control_reg;

typedef struct mb_WR_regs_map_struct {
	control_reg control;//Control register (Start_Addr:40001)
	unsigned short RES_0[8];
	fls_app_config config;//Configuration registers (Start_Addr:40010)
} mb_RDWR_regs;

#pragma pack(pop)//Disable packing

//struct for system_stats
struct system_stats {
	float CPU_Util, RAM_Util, CPU_temp, Disk_Util;
	unsigned int Up_time_days, Up_time_hours, Up_time_min, Up_time_sec;
};

//Struct for thread argument passing
struct arg_passer {
	mb_RD_regs *mb_RD;
	mb_RDWR_regs *mb_RDWR;
};

//Struct for MODBus-TCP query header
typedef struct MODBus_TCP_query_struct {
	unsigned short trans_ID;
	unsigned short protocol_ID;
	unsigned short length;
	unsigned char  unit_ID;
	unsigned char  func_code;
	union MODBus_TCP_query_Data{
		struct mb_write_reg_struct{
			unsigned short reg_addr;
			unsigned short value;
		} __attribute__ ((packed)) mb_write_reg;
		struct mb_mask_write_reg_struct{
			unsigned short reg_addr;
			unsigned short And_mask;
			unsigned short Or_mask;
		} __attribute__ ((packed)) mb_mask_write_reg;
		struct mb_write_regs_struct{
			unsigned short start_write_reg_addr;
			unsigned short amount_of_regs_to_write;
			unsigned char  write_byte_count;
			unsigned short data[MODBUS_MAX_WRITE_REGISTERS];
		} __attribute__ ((packed)) mb_write_regs;
		struct mb_read_write_regs_struct{
			unsigned short start_read_reg_addr;
			unsigned short amount_of_regs_to_read;
			unsigned short start_write_reg_addr;
			unsigned short amount_of_regs_to_write;
			unsigned char  write_byte_count;
			unsigned short data[MODBUS_MAX_WR_WRITE_REGISTERS];
		} __attribute__ ((packed)) mb_read_write_regs;
	} data;
} MODBus_TCP_query;
#endif //FLS_APP_TYPES_H
